import path from "path";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/shih-tzu/",

  build: {
    emptyOutDir: false,
    lib: {
      entry: path.resolve(__dirname, "src", "main.tsx"),
      fileName: "shih-tzu",
      name: "Gamesite6_ShihTzu",
    },
  },
  plugins: [react()],
});
