type PlayerId = number;

declare namespace JSX {
  interface IntrinsicElements {
    "gs6-user": {
      key?: string | number;
      compact?: boolean;
      playerid: number;
      suffix?: string;
    };
    "gs6-avatar": { key?: string | number; playerid: number; inline?: boolean };
    "gs6-player-box": {
      key?: string | number;
      playerid: number;
      thinking?: boolean;
      speaking?: boolean;
      children?: any;
    };
  }
}
