import type { GameSettings, GameState } from "./shared";
import * as React from "react";
import * as ReactDOM from "react-dom";
import GameComponent from "./components/Game";
import ReferenceComponent from "./components/Reference";
import SettingsComponent from "./components/Settings";
export { defaultSettings, playerCounts } from "./components/Settings";

import "./game.css";

export class Game extends HTMLElement {
  #state?: GameState;
  #settings?: GameSettings;
  #playerId?: PlayerId;

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      this.render();
    }
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.render();
    }
  }

  set playerid(playerId: PlayerId | undefined) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      this.render();
    }
  }

  private render() {
    if (this.#settings && this.#state) {
      ReactDOM.render(
        <GameComponent
          userId={this.#playerId}
          settings={this.#settings}
          state={this.#state}
          onAction={(action) => {
            this.dispatchEvent(
              new CustomEvent("action", {
                detail: action,
                bubbles: false,
              })
            );
          }}
        />,
        this
      );
    }
  }

  connectedCallback() {
    this.render();
  }
}

export class Reference extends HTMLElement {
  _settings?: GameSettings;

  set settings(settings: GameSettings) {
    this._settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  private render() {
    if (this._settings) {
      ReactDOM.render(<ReferenceComponent settings={this._settings} />, this);
    }
  }

  connectedCallback() {
    this.render();
  }

  static get observedAttributes() {
    return ["settings"];
  }

  attributeChangedCallback() {
    this.render();
  }
}

export class Settings extends HTMLElement {
  #settings?: GameSettings;

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.render();
    }
  }

  private render() {
    ReactDOM.render(
      <SettingsComponent
        settings={this.#settings}
        readonly={this.#readonly}
        onSettingsChange={(settings: GameSettings) => {
          this.dispatchEvent(
            new CustomEvent("settings_change", {
              detail: settings,
            })
          );
        }}
      />,
      this
    );
  }

  connectedCallback() {
    this.render();
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly") {
      this.render();
    }
  }
}
