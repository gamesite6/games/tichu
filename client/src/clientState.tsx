import { Card, Player, PlayerId, GameState } from "./shared";

export type ClientState = PushingState | PlayingState | Empty;

export type PushingState = {
  kind: "Pushing";
  userId?: PlayerId;
  selected?: PlayerId;
  pushed: { [playerId: number]: Card };
};

export type PlayingState = {
  kind: "Playing";
  userId?: PlayerId;
  selected: Card[];
  wish?: number;
};

export type Empty = {
  kind: "Empty";
  userId?: PlayerId;
};

export function getInitialClientState(
  state: GameState,
  user?: Player
): ClientState {
  if (!user) {
    return { kind: "Empty" };
  }

  switch (state.phase.kind) {
    case "BigBetting":
    case "Receiving":
    case "RoundComplete":
    case "GameComplete": {
      return { kind: "Empty", userId: user.id };
    }

    case "Pushing": {
      const firstOtherPlayer = state.players.find(
        (player) => player.id !== user.id
      );
      return {
        userId: user?.id,
        kind: "Pushing",
        selected: firstOtherPlayer?.id,
        pushed: {},
      };
    }

    case "Leading":
    case "Following": {
      return {
        kind: "Playing",
        userId: user?.id,
        selected: [],
      };
    }
  }
}

export function cardIntersection(
  clientState: ClientState,
  hand: Card[]
): ClientState | null {
  switch (clientState.kind) {
    case "Empty":
    case "Pushing":
      return null;

    case "Playing": {
      function isInHand(card: Card) {
        return hand.includes(card);
      }

      if (clientState.selected.some((card) => !isInHand(card))) {
        return {
          ...clientState,
          selected: clientState.selected.filter(isInHand),
        };
      } else {
        return null;
      }
    }
  }
}
