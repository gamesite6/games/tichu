import {
  Card,
  Player,
  PlayerId,
  GameState,
  SpecialCard,
  phoenix,
} from "./shared";

export function sortCards(cards: Card[]): Card[] {
  const cards2 = [...cards];
  cards2.sort((a, b) => a - b);
  return cards2;
}

export function sortPlayers(
  userId: PlayerId | undefined,
  players: Player[]
): Player[] {
  const userIdx = players.findIndex((player) => player.id === userId);
  if (userIdx === -1) {
    return players;
  } else {
    return [...players.slice(userIdx), ...players.slice(0, userIdx)];
  }
}

export function playerIsThinking(
  state: GameState,
  playerId: PlayerId
): boolean {
  switch (state.phase.kind) {
    case "BigBetting":
      return !state.phase.ready.includes(playerId);
    case "Pushing":
      return !state.phase.pushed[playerId];
    case "Receiving":
      return !state.phase.ready.includes(playerId);
    case "Leading":
      return state.phase.player === playerId;
    case "Following":
      return state.phase.player === playerId;
    case "RoundComplete":
      return !state.phase.ready.includes(playerId);
    case "GameComplete":
      return false;
  }
}

export function formatPointChange(points: number): string {
  if (points === 0) {
    return "—";
  } else if (points > 0) {
    return `+${points}`;
  } else {
    return `−${Math.abs(points)}`;
  }
}

export function formatPoints(points: number): string {
  if (points < 0) {
    return `−${Math.abs(points)}`;
  } else {
    return points.toString();
  }
}

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";

export function getSpecialCardValueStyle(props: { card: SpecialCard }) {
  switch (props.card) {
    case phoenix:
      return css`
        filter: sepia(1) saturate(2.5) hue-rotate(340deg);
      `;
    default:
      return "";
  }
}

export function getRankText(rank: number): string {
  switch (rank) {
    case 14:
      return "A";
    case 13:
      return "K";
    case 12:
      return "Q";
    case 11:
      return "J";
    default:
      return rank.toString();
  }
}
