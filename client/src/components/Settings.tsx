import { GameLength, GameSettings, Tiebreaker } from "../shared";
import { useState } from "react";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";

type Props = {
  settings?: GameSettings;
  readonly: boolean;
  onSettingsChange: (settings: GameSettings) => void;
};

const gameLengthOptions: Array<[GameLength, string, string]> = [
  ["standard-game", "Standard game", "Reach 1000 points to win."],
  ["shorter-game", "Shorter game", "Reach 500 points to win."],
  ["single-round", "Single round", "Highest score after a single round wins."],
];

const tiebreakerOptions: Array<[Tiebreaker, string]> = [
  ["play-another-round", "Play another round"],
  ["no-tiebreaker", "None"],
];

export default function Settings(props: Props) {
  const [uniqueSuffix, _] = useState(
    "-" + (Math.random() + 1).toString(36).substring(7)
  );
  const { settings = defaultSettings } = props;

  return (
    <section
      css={css`
        legend {
          font-weight: 500;
        }
      `}
    >
      <fieldset css={fieldsetCss}>
        <legend>Number of players</legend>

        <label>
          <input type="checkbox" checked disabled /> 4
        </label>

        <div
          css={css`
            display: flex;
            gap: 1rem;
          `}
        >
          <small>Recommended: 4 </small>
          <small>Best: 4</small>
        </div>
      </fieldset>

      <fieldset css={fieldsetCss}>
        <legend>
          <label>Game length</label>
        </legend>

        <ul
          css={css`
            margin: 0;
            display: flex;
            flex-direction: column;
            gap: 0.5rem;

            label {
              display: flex;
              align-items: baseline;
              gap: 0.25rem;
            }
            label > div {
              display: flex;
              flex-direction: column;
            }
          `}
        >
          {gameLengthOptions.map(([key, text, subtext]) => (
            <li key={key}>
              <label htmlFor={key + uniqueSuffix}>
                <input
                  type="radio"
                  name={"game-length" + uniqueSuffix}
                  id={key + uniqueSuffix}
                  disabled={props.readonly}
                  checked={settings.gameLength === key}
                  onChange={(_) => {
                    props.onSettingsChange({ ...settings, gameLength: key });
                  }}
                />
                <div>
                  <div>{text}</div>
                  <small>{subtext}</small>
                </div>
              </label>
            </li>
          ))}
        </ul>
      </fieldset>

      <fieldset css={fieldsetCss}>
        <legend>Tiebreaker</legend>

        <ul
          css={css`
            margin: 0;
            display: flex;
            flex-direction: column;

            label {
              display: flex;
              align-items: baseline;
              gap: 0.25rem;
            }
          `}
        >
          {tiebreakerOptions.map(([key, text]) => (
            <li key={key}>
              <label htmlFor={key + uniqueSuffix}>
                <input
                  type="radio"
                  name={"tiebreaker" + uniqueSuffix}
                  id={key + uniqueSuffix}
                  checked={settings.tiebreaker === key}
                  disabled={props.readonly}
                  onChange={(_) => {
                    props.onSettingsChange({ ...settings, tiebreaker: key });
                  }}
                />
                <span>{text}</span>
              </label>
            </li>
          ))}
        </ul>
      </fieldset>
    </section>
  );
}

const fieldsetCss = css`
  border: 0;
  padding: 0;
  margin: 1rem 0;
`;

export const defaultSettings: GameSettings = {
  gameLength: "standard-game",
  tiebreaker: "play-another-round",
};

export function playerCounts(settings: GameSettings): number[] {
  return [4];
}
