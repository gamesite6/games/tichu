import {
  ActionHandler,
  Card,
  getScoreValue,
  Player,
  PlayerId,
  RoundComplete,
  Team,
} from "../shared";
import { sum } from "lodash";
import * as React from "react";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { formatPointChange, formatPoints } from "../util";

type HtmlAttrs = React.HTMLAttributes<HTMLElement>;
type Props = {
  teams: [Team, Team];
  phase: RoundComplete;
  onAction: ActionHandler;
  user?: Player;
};

export function RoundSummary(props: Props & HtmlAttrs) {
  const { teams, phase, onAction, user, ...htmlAttrs } = props;

  const doubleVictory = phase.finished.length === 2;

  return (
    <section
      css={css`
        position: relative;
        height: 100%;
        text-align: center;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
      `}
    >
      <h2
        css={css`
          margin-bottom: 1rem;
        `}
      >
        {doubleVictory ? "Double Victory!" : "Round Summary"}
      </h2>

      {doubleVictory ? (
        <DoubleVictoryTable teams={teams} finished={phase.finished} />
      ) : (
        <NormalTable teams={teams} finished={phase.finished} />
      )}

      <div
        css={css`
          height: 3em;
          margin-top: 2em;
        `}
      >
        {user && !phase.ready.includes(user.id) && (
          <button
            className="primary"
            onClick={() => onAction({ kind: "Ready" })}
          >
            OK
          </button>
        )}
      </div>
    </section>
  );
}

const tableCss = css`
  border-collapse: collapse;

  td {
    padding-top: 0.4em;
    padding-bottom: 0.4em;
  }

  tr:nth-child(3) > td {
    border-top: solid lightgray 1px;
  }

  > thead > tr > th {
    border-bottom: solid gray 2px;
    min-width: 5em;
  }

  > tbody > tr > td:first-child {
    text-align: left;
  }
`;

function DoubleVictoryTable(props: {
  teams: [Team, Team];
  finished: PlayerId[];
}) {
  return (
    <table css={tableCss}>
      <thead>
        <tr>
          <th>Team</th>
          <th>Cards</th>
          <th>Bet</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {props.teams.map((team) => {
          const victors = team.members.some((p) =>
            props.finished.includes(p.id)
          );
          const victoryPoints = victors ? 200 : 0;
          const betPoints = team.members.map((p) => {
            const value = p.bet ?? 0;
            return p.id === props.finished[0] ? value : -value;
          });
          const betTotal = sum(betPoints);

          const newTeamScore = team.score + victoryPoints + betTotal;

          return (
            <React.Fragment key={team.members.map((p) => p.id).join("-")}>
              <tr>
                <td>
                  <gs6-user playerid={team.members[0].id} />
                </td>
                <td rowSpan={2}>{formatPointChange(victoryPoints)}</td>
                <td>{formatPointChange(betPoints[0])}</td>
                <td rowSpan={2}>
                  {team.score !== newTeamScore ? (
                    <div css={strickenPointsCss}>
                      {formatPoints(team.score)}
                    </div>
                  ) : null}

                  <strong>{formatPoints(newTeamScore)}</strong>
                </td>
              </tr>
              <tr>
                <td>
                  <gs6-user playerid={team.members[1].id} />
                </td>

                <td>{formatPointChange(betPoints[1])}</td>
              </tr>
            </React.Fragment>
          );
        })}
      </tbody>
    </table>
  );
}

function NormalTable(props: { teams: [Team, Team]; finished: PlayerId[] }) {
  const { teams } = props;

  const players = [...teams[0].members, ...teams[1].members];
  const winner = players.find((p) => p.id === props.finished[0]) as Player;

  const loserIndex = players.findIndex((p) => p.hand.length !== 0);

  const loser = players[loserIndex];

  const losingTeam = teams.find((team) => team.members.includes(loser));
  const winningTeam = teams.find((team) => team !== losingTeam);

  const loserHandPoints = sum(loser.hand.map(getScoreValue));

  return (
    <table css={tableCss}>
      <caption
        css={css`
          text-align: left;
          margin-bottom: 1em;
        `}
      >
        <p>
          <gs6-user playerid={loser.id} /> lost the round, and gives the cards
          in their hand to their opponents.
        </p>
        <p>
          <gs6-user playerid={winner.id} /> won the round, and receives{" "}
          <gs6-user playerid={loser.id} suffix="'s" /> claimed cards.
        </p>
      </caption>
      <thead>
        <tr>
          <th>Team</th>
          <th>Cards</th>
          <th>Bet</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {props.teams.map((team) => {
          const teamClaimedCards: Card[][] = team.members.map((p) => {
            if (p.id === winner.id) {
              return [...p.claimed, ...loser.claimed];
            } else if (p.id === loser.id) {
              return [];
            } else {
              return p.claimed;
            }
          });

          const cardPoints = teamClaimedCards.map((cards) =>
            sum(cards.map(getScoreValue))
          );
          const cardTotal = sum(cardPoints);
          const betPoints = team.members.map((p) => {
            const value = p.bet ?? 0;
            return p.id === props.finished[0] ? value : -value;
          });
          const betTotal = sum(betPoints);

          const newTeamScore =
            team.score +
            cardTotal +
            betTotal +
            (team === winningTeam ? loserHandPoints : 0);

          return (
            <React.Fragment key={team.members.map((p) => p.id).join("-")}>
              {team.members.map((member, memberIndex) => (
                <tr key={member.id}>
                  <td>
                    <gs6-user playerid={member.id} />
                  </td>
                  <td
                    css={css`
                      position: relative;
                    `}
                  >
                    {member.id === loser.id || member.id === winner.id ? (
                      <strong>
                        {formatPointChange(cardPoints[memberIndex])}
                      </strong>
                    ) : (
                      formatPointChange(cardPoints[memberIndex])
                    )}

                    {team === winningTeam && memberIndex === 0 ? (
                      <small
                        css={css`
                          position: absolute;
                          inset: 0;
                          transform: translateY(50%);
                          display: flex;
                          justify-content: center;
                          align-items: center;
                        `}
                      >
                        {formatPointChange(loserHandPoints)}
                      </small>
                    ) : null}
                  </td>
                  <td>{formatPointChange(betPoints[memberIndex])}</td>

                  {memberIndex === 0 ? (
                    <td rowSpan={2}>
                      {team.score !== newTeamScore ? (
                        <div css={strickenPointsCss}>
                          {formatPoints(team.score)}
                        </div>
                      ) : null}

                      <strong>{formatPoints(newTeamScore)}</strong>
                    </td>
                  ) : null}
                </tr>
              ))}
            </React.Fragment>
          );
        })}
      </tbody>
    </table>
  );
}

const strickenPointsCss = css`
  text-decoration: line-through;
  filter: opacity(50%);
  font-weight: 500;

  :before,
  :after {
    content: " ";
  }
`;
