import {
  ActionHandler,
  Following,
  getTeams,
  BigBetting,
  dog,
  Leading,
  Player,
  Pushing,
  Receiving,
  GameState,
  Team,
  sparrow,
  Phase,
} from "../shared";
import produce from "immer";
import { maxBy } from "lodash";
import { ClientState, PushingState } from "../clientState";
import { CardEmpty, CardFace, SpecialCardIcon } from "./Card";
import * as React from "react";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { getRankText } from "../util";
import { Wish } from "./Wish";

type Props = {
  state: GameState;
  sortedPlayers: Player[];
  user?: Player;
  onAction: ActionHandler;
  clientState: ClientState;
  setClientState: (cs: ClientState) => any;
};
export function Center(props: Props) {
  return (
    <div
      css={css`
        min-width: 15em;
        display: grid;
        place-content: center;
        text-align: center;
        ${props.user?.hand.length
          ? css`
              /* move center component up so that it's visible over the user's cards. */
              transform: translateY(-2.5rem);
            `
          : ""}
      `}
    >
      <PhaseC {...props} />
    </div>
  );
}

function PhaseC(props: Props) {
  const { state, sortedPlayers, onAction, user, clientState } = props;
  switch (state.phase.kind) {
    case "BigBetting":
      return (
        <BigBettingC phase={state.phase} onAction={onAction} user={user} />
      );

    case "Pushing":
      return (
        <PushingC
          players={sortedPlayers}
          phase={state.phase}
          clientState={
            clientState?.kind === "Pushing" ? clientState : undefined
          }
          setClientState={props.setClientState}
          user={user}
          onAction={onAction}
        />
      );
    case "Receiving":
      return (
        <ReceivingC
          players={sortedPlayers}
          phase={state.phase}
          user={user}
          onAction={onAction}
        />
      );
    case "Leading":
      return <LeadingC phase={state.phase} wish={state.wish} user={user} />;
    case "Following":
      return <FollowingC phase={state.phase} wish={state.wish} user={user} />;
    case "RoundComplete": {
      return null;
    }
    case "GameComplete": {
      return <GameCompleteC teams={getTeams(state)} />;
    }
  }
}

function BigBettingC(props: {
  phase: BigBetting;
  onAction: ActionHandler;
  user?: Player;
}) {
  if (!props.user || props.phase.ready.includes(props.user.id)) {
    return (
      <p>
        <em>Players are deciding whether to bet.</em>
      </p>
    );
  }

  return (
    <>
      <p>
        <em>
          You may place a big bet <br /> before seeing the rest of your cards.
        </em>
      </p>
      <div
        css={css`
          white-space: nowrap;
        `}
      >
        <button
          className="secondary"
          onClick={() => props.onAction({ kind: "Pass" })}
        >
          Pass
        </button>{" "}
        <button
          className="primary"
          onClick={() => props.onAction({ kind: "BetBig" })}
        >
          Bet 200 points
        </button>
      </div>
    </>
  );
}

const playerListCss = css`
  display: flex;
  > * {
    margin: 0.5em;
    :first-child {
      margin-left: 0;
    }
    :last-child {
      margin-right: 0;
    }
  }
`;

const pushingPlayerCss = css`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const emptyCardCss = css`
  margin-top: 0.5rem;
  font-size: 0.5em;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const pushedCardCss = css`
  margin-top: 0.5rem;
  display: block;
  font-size: 0.5em;
  border: none;
  padding: 0;
  background: transparent;
  color: inherit;

  :not(:disabled) {
    cursor: pointer;
  }
`;

function PushingC(props: {
  players: Player[];
  phase: Pushing;
  clientState?: PushingState;
  setClientState: (cs: PushingState) => any;
  user?: Player;
  onAction: ActionHandler;
}) {
  const { user, clientState, phase } = props;

  const otherPlayers = props.players.filter((player) => player.id !== user?.id);

  if (!user || !clientState) return <em>Players are pushing cards.</em>;

  let selected = clientState.selected;
  const userHasPushed = props.phase.pushed[user.id];
  const mayBet = !user.bet;

  return (
    <div
      css={css`
        position: relative;
      `}
    >
      <div
        css={css`
          position: absolute;

          top: 0;

          width: 100%;
          transform: translateY(-100%) translateY(-1rem);
        `}
      >
        {userHasPushed ? (
          <em>You pushed:</em>
        ) : (
          <em>Push a card to each player.</em>
        )}
      </div>

      <div css={playerListCss}>
        {otherPlayers.map((player, playerIdx) => {
          const pushedCard =
            (userHasPushed
              ? phase.pushed[user.id]?.[player.id]
              : clientState.pushed?.[player.id]) ?? null;
          return (
            <div
              css={pushingPlayerCss}
              key={`pushing-from-${user.id}-to-${player.id}`}
            >
              <label>
                {playerIdx === 0 ? "←" : playerIdx === 1 ? "↑" : "→"}{" "}
                {!userHasPushed ? (
                  <input
                    type="radio"
                    name="push-player"
                    checked={selected === player.id}
                    disabled={userHasPushed}
                    onChange={() =>
                      props.setClientState({
                        ...clientState,
                        selected: player.id,
                      })
                    }
                  />
                ) : null}{" "}
                <gs6-user compact playerid={player.id} />
                {pushedCard === null && (
                  <div css={emptyCardCss}>
                    <CardEmpty />
                  </div>
                )}
              </label>
              {pushedCard !== null && (
                <button
                  css={pushedCardCss}
                  disabled={!!userHasPushed}
                  onClick={() => {
                    const nextClientState = produce(clientState, (draft) => {
                      delete draft.pushed[player.id];
                    });
                    props.setClientState(nextClientState);
                  }}
                >
                  <CardFace card={pushedCard} />
                </button>
              )}
            </div>
          );
        })}
      </div>
      <div
        css={css`
          position: absolute;
          bottom: 0;
          transform: translateY(100%) translateY(1rem);
          width: 100%;
          display: flex;
          justify-content: center;
          gap: 0.25rem;
        `}
      >
        {mayBet ? (
          <button
            className="secondary"
            onClick={() => props.onAction({ kind: "BetSmall" })}
          >
            Bet 100 points
          </button>
        ) : null}
        {!userHasPushed ? (
          <button
            className="primary"
            disabled={Object.values(clientState.pushed).length < 3}
            onClick={() =>
              props.onAction({ kind: "Push", pushed: clientState.pushed })
            }
          >
            Push
          </button>
        ) : null}
      </div>
    </div>
  );
}

function ReceivingC(props: {
  players: Player[];
  phase: Receiving;
  user?: Player;
  onAction: ActionHandler;
}) {
  const { user } = props;
  const otherPlayers = props.players.filter((player) => player.id !== user?.id);
  if (!user || props.phase.ready.includes(user.id)) {
    return (
      <p>
        <em>Players are receiving pushed cards.</em>
      </p>
    );
  }
  return (
    <div
      css={css`
        position: relative;
      `}
    >
      <div
        css={css`
          position: absolute;
          top: 0;
          width: 100%;
          transform: translateY(-100%) translateY(-1rem);
        `}
      >
        <em>You received cards!</em>
      </div>

      <div css={playerListCss}>
        {otherPlayers.map((player) => (
          <div
            css={pushingPlayerCss}
            key={`${user.id}-received-from-${player.id}}`}
          >
            <gs6-user compact playerid={player.id} />
            <button css={pushedCardCss} disabled>
              <CardFace card={props.phase.pushed[player.id][user.id]} />
            </button>
          </div>
        ))}
      </div>

      <div
        css={css`
          position: absolute;
          bottom: 0;
          width: 100%;
          transform: translateY(100%) translateY(1rem);
        `}
      >
        {props.phase.ready.includes(user.id) ? null : (
          <button
            className="primary"
            onClick={() => props.onAction({ kind: "Ready" })}
          >
            OK
          </button>
        )}
      </div>
    </div>
  );
}

function LeadingC(props: { phase: Leading; user?: Player; wish?: number }) {
  const { user, phase, wish } = props;
  return (
    <div
      css={css`
        position: relative;
        width: 20rem;
      `}
    >
      <div
        css={css`
          position: absolute;
          top: 0;
          width: 100%;
          transform: translateY(-100%) translateY(-1rem);
        `}
      >
        {user && phase.player === user.id ? (
          <em>You must lead with a card combination.</em>
        ) : (
          <em>
            <gs6-user playerid={phase.player} /> must lead with a card
            combination.
          </em>
        )}
      </div>
      {phase.dog ? (
        <div
          css={css`
            display: inline-block;
            font-size: 0.75em;
          `}
        >
          <CardFace card={dog} />
        </div>
      ) : null}
      <div
        css={css`
          position: absolute;
          bottom: 0;
          width: 100%;
          transform: translateY(100%) translateY(1rem);
        `}
      >
        {wish ? <Wish wish={wish} phase={phase} user={user} /> : null}
      </div>
    </div>
  );
}

type ComboProps = { idx: number; size: number };

const playedComboCss = (props: ComboProps) => css`
  font-size: 0.6em;
  position: absolute;
  :last-child {
    font-size: 0.75em;
    position: relative;
  }

  display: flex;

  top: ${(1 + props.idx - props.size) * 4.1}em;
  left: 50%;
  transform: translateX(-50%) translateX(${(1 + props.idx - props.size) * 6}em);

  > * {
    margin-left: -3.8em;
    margin-right: -3.8em;
    :first-child {
      margin-left: 0;
    }
    :last-child {
      margin-right: 0;
    }
  }
`;

function FollowingC({
  phase,
  wish,
  user,
}: {
  phase: Following;
  wish?: number;
  user?: Player;
}) {
  return (
    <div
      css={css`
        position: relative;
      `}
    >
      <div
        css={css`
          position: relative;
        `}
      >
        {phase.cards.map((combo, idx) => {
          const key = combo.join("-");

          return (
            <div
              key={key}
              css={playedComboCss({ idx, size: phase.cards.length })}
            >
              {combo.map((card) => (
                <CardFace key={card} card={card} />
              ))}
            </div>
          );
        })}
      </div>
      <div
        css={css`
          position: absolute;
          left: 50%;
          top: calc(100% + 0.5rem);
          transform: translateX(-50%);
          white-space: nowrap;
        `}
      >
        Played by <gs6-user playerid={phase.leader} />
        {wish ? <Wish wish={wish} phase={phase} user={user} /> : null}
      </div>
    </div>
  );
}

function GameCompleteC(props: { teams: [Team, Team] }) {
  const { teams } = props;
  const winningTeam = maxBy(teams, (team) => team.score) as Team;
  const isDraw = teams[0].score === teams[1].score;
  const [player1, player2] = winningTeam.members;
  return (
    <div
      css={css`
        font-size: 1.5em;
        max-width: 24rem;
      `}
    >
      {isDraw ? (
        <span>It's a draw!</span>
      ) : (
        <span>
          <gs6-user playerid={player1.id} /> and{" "}
          <gs6-user playerid={player2.id} /> win!
        </span>
      )}
    </div>
  );
}
