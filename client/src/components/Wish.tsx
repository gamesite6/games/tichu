import {
  Following,
  Leading,
  Player,
  sparrow,
  mustFulfillWish,
} from "../shared";
import { getRankText } from "../util";
import { SpecialCardIcon } from "./Card";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";

type Props = { wish: number; phase: Leading | Following; user?: Player };
export function Wish({ wish, phase, user }: Props) {
  const rank = getRankText(wish);

  return (
    <div
      css={css`
        margin: 0.5rem 0;
        display: flex;
        justify-content: center;
        align-items: center;
      `}
    >
      <div
        className="shadow border"
        css={css`
          position: relative;
          border-radius: 0.5em;
          background-color: white;
          padding: 0.25em 0.75em;
          margin-right: 0.5em;

          ::before {
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            bottom: 100%;
            right: 0;
            transform: translateX(90%);
            top: 0.5em; // offset should move with padding of parent
            border: 0.5em solid transparent;
            filter: drop-shadow(0.175em 0.15em 0.075em #0004);

            // looks
            border-left-color: white;
          }
        `}
      >
        {user && mustFulfillWish(phase, user, wish) ? (
          <span>
            You can play a <strong>{rank}</strong> and <em>must</em> do so!
          </span>
        ) : (
          <span>
            <strong>{rank}</strong> <em>must</em> be played if possible!
          </span>
        )}
      </div>
      <SpecialCardIcon card={sparrow} />
    </div>
  );
}
