import { dog, dragon, GameSettings, phoenix, sparrow } from "../shared";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { SpecialCardIcon } from "./Card";

import { useRef } from "react";
import type { MouseEvent } from "react";

type Props = {
  settings: GameSettings;
};

export default function Game(props: Props) {
  const section = useRef(null);

  function handleLinkClick(ev: MouseEvent) {
    ev.preventDefault();
    const query = (ev.target as Element).getAttribute("href");

    if (query) {
      (section.current as unknown as Element)
        ?.querySelector(query)
        ?.scrollIntoView();
    }
  }

  return (
    <section
      ref={section}
      className="panel"
      css={css`
        height: 100%;
        overflow: auto;
        box-sizing: border-box;
        background-color: linen;
        font-size: 85%;

        h2 {
          font-size: 125%;
        }

        summary {
          cursor: pointer;
        }

        .card-rank {
          display: inline-block;
          font-size: 1.2em;
          line-height: 1;
          margin: 0 0.125em;
          font-family: "Caveat Brush", cursive;
        }

        .special-card-name {
          font-weight: 500;
        }
      `}
    >
      <h2>Rules reference</h2>

      <details open>
        <summary>Game play</summary>
        <p>
          The game is played by two teams of two players, with team members
          sitting opposite one another.
        </p>

        <p>
          After being dealt 8 cards, a player may place a big bet before seeing
          the rest of their cards. If this is your first time playing you can
          safely pass without worrying about it. See{" "}
          <a onClick={handleLinkClick} href="#bets">
            Bets
          </a>{" "}
          for more information.
        </p>

        <p>
          After being dealt all their cards, each player pushes 1 card to each
          other player.
        </p>

        <p>
          The player with the <SpecialCardIcon card={sparrow} />{" "}
          <span className="special-card-name">Sparrow</span> begins the round by{" "}
          <em>leading</em> with any valid{" "}
          <a onClick={handleLinkClick} href="#combinations">
            card combination
          </a>
          .
        </p>

        <p>
          Then, the players take turns either passing, or <em>following</em>{" "}
          with a higher value combination of the same type. If every other
          player passes, whoever played the highest value combination wins the{" "}
          <em>trick</em>, and claims all played cards (and{" "}
          <a onClick={handleLinkClick} href="#scoring">
            points
          </a>
          ). The winner then begins a new trick by leading with another
          combination of their choice.
        </p>
        <p>
          Play continues like this until all but one player have played all
          their cards. The round is then{" "}
          <a onClick={handleLinkClick} href="#scoring">
            scored
          </a>
          .
        </p>
      </details>

      <details open>
        <summary>Special cards</summary>

        <dl css={definitionListCss}>
          <dt>
            <SpecialCardIcon card={dragon} /> Dragon
          </dt>
          <dd>
            <p>
              The Dragon is the highest ranking card in the deck, a full rank
              higher than an <span className="card-rank">A</span>. It can only
              be played as a single card, but then can only be beaten by a{" "}
              <a onClick={handleLinkClick} href="#bombs">
                bomb
              </a>
              .
            </p>
            <p>
              The Dragon is worth 25 points, however, if the Dragon wins a
              trick, the player must give the cards to one of their opponents.
            </p>
          </dd>

          <dt>
            <SpecialCardIcon card={phoenix} /> Phoenix
          </dt>
          <dd>
            <p>
              The Phoenix can be played as a wild card in a card combination,
              representing any other normal card.
            </p>
            <p>
              The Phoenix can also be played as a single card. In which case it
              will have a rank 0.5 higher than the previously played card. If
              leading with the Phoenix, it will have a rank of 1.5.
            </p>
            <p>
              The Phoenix is a very powerful card, however, it is worth -25
              points.
            </p>
          </dd>

          <dt>
            <SpecialCardIcon card={sparrow} /> Sparrow
          </dt>
          <dd>
            <p>
              Whoever has the Sparrow begins the round, whoever they don't have
              to play the Sparrow immediately.
            </p>
            <p>The Sparrow has a rank of 1, and can be used in a straight.</p>
            <p>
              When the Sparrow is played, the player can wish for a certain rank
              between <span className="card-rank">2</span> and{" "}
              <span className="card-rank">A</span>. The next player able to play
              a combination containing that rank on their turn, must do so!
            </p>
          </dd>

          <dt>
            <SpecialCardIcon card={dog} /> Dog
          </dt>
          <dd>
            <p>
              The Dog has no rank at all, and can only be played by leading with
              it as a single card.
            </p>
            <p>When played, it passes the turn to the player's partner.</p>
          </dd>
        </dl>
      </details>

      <details open>
        <summary id="combinations">Card combinations</summary>
        <dl css={definitionListCss}>
          <dt>Single</dt>
          <dd>
            Any single card, e.g. <span className="card-rank">5</span>.
          </dd>

          <dt>Pair</dt>
          <dd>
            Two cards of the same rank, e.g.{" "}
            <span className="card-rank">7, 7</span>.
          </dd>

          <dt>Three of a kind</dt>
          <dd>
            Three cards of the same rank, e.g.{" "}
            <span className="card-rank">J, J, J</span>.
          </dd>

          <dt>Straight</dt>
          <dd>
            <p>
              At least 5 cards of connected ranks, e.g.
              <span
                css={css`
                  white-space: nowrap;
                `}
              >
                <span className="card-rank">
                  <SpecialCardIcon card={sparrow} />, 2, 3, 4, 5
                </span>
              </span>{" "}
              or <span className="card-rank">7, 8, 9, 10, J, Q, K, A</span>.
            </p>
            <p>
              When following a straight, you must use the same number of cards
              as the previously played straight.
            </p>
          </dd>

          <dt>Straight pairs</dt>
          <dd>
            <p>
              At least 2 pairs of connected ranks, e.g.
              <span
                css={css`
                  white-space: nowrap;
                `}
              >
                <span className="card-rank">5, 5, 6, 6</span>
              </span>{" "}
              or <span className="card-rank">9, 9, 10, 10, J, J</span>.
            </p>
            <p>
              When following straight pairs, you must use the same number of
              cards as the previously played straight pairs.
            </p>
          </dd>

          <dt>Full house</dt>
          <dd>
            <p>
              A pair and three of a kind, e.g.
              <span className="card-rank">2, 2, 8, 8, 8</span> or{" "}
              <span className="card-rank">Q, Q, 6, 6, 6</span>.
            </p>
          </dd>
        </dl>
      </details>

      <details open>
        <summary id="bombs">Bombs</summary>
        <p>
          A bomb is a special kind of card combination that can can be played at
          any time, even out of turn, on top of any previous card combination.
          However, if played after another bomb, it must be of higher value.
        </p>
        <p>
          The value of a bomb is determined first by the number of cards, and
          secondly by the rank of the cards. Special cards, such as the Sparrow
          or the Phoenix, may not be part of a bomb.
        </p>
        <p>There are two different kinds of bombs:</p>
        <dl css={definitionListCss}>
          <dt>Four of a kind</dt>
          <dd>All four cards of a rank.</dd>

          <dt>Straight flush</dt>
          <dd>At least 5 cards of connected ranks of the same suit.</dd>
        </dl>
      </details>

      <details open>
        <summary id="scoring">Scoring</summary>
        <p>
          The loser of the round (the last player with cards) gives their
          remaining cards to his opponents, and any claimed tricks to the
          winner.
        </p>
        <p>
          The players then score points based on the cards they have claimed.
        </p>
        <ul>
          <li>
            10 points for each <span className="card-rank">K</span> and{" "}
            <span className="card-rank">10</span>.
          </li>
          <li>
            5 points for each <span className="card-rank">5</span>.
          </li>
          <li>
            25 points for the <SpecialCardIcon card={dragon} />{" "}
            <span className="special-card-name">Dragon</span>.
          </li>
          <li>
            -25 points for the <SpecialCardIcon card={phoenix} />{" "}
            <span className="special-card-name">Phoenix</span>.
          </li>
        </ul>
        <p>
          In other words, there are <strong>100</strong> points up for grabs
          each round.
        </p>
        <p>
          However, if both players of a team are the two first players to play
          all their cards, the card scoring is skipped and that team is instead
          awarded <strong>200</strong> points.
        </p>
      </details>

      <details open>
        <summary id="bets">Bets</summary>

        <p>
          A player may place a <em>bet</em> at certain points of a round. There
          are two possible bets:
        </p>
        <dl css={definitionListCss}>
          <dt>Big bet</dt>

          <dd>
            After being dealt only 8 cards, a player may place a big bet. If the
            betting player wins the round (i.e. is the first to play all their
            cards) their team gains 200 points. However, if they fail to win,
            their team instead lose 200 points.
          </dd>

          <dt>Small bet</dt>
          <dd>
            Before playing their first card, a player may place a small bet. If
            the betting player wins the round their team gains 100 points. If
            they fail to win, their team lose 100 points.
          </dd>

          <p>
            Both players of a team may place a bet in the same round, however
            only one of them can win.
          </p>
        </dl>
      </details>

      <details open>
        <summary id="game-end">End of the game</summary>
        <p>
          <em>(This section depends on game settings.)</em>
        </p>

        <p>
          {(() => {
            switch (props.settings.gameLength) {
              case "shorter-game":
                return (
                  <span>
                    The game ends once any team has reached 500 points.
                  </span>
                );
              case "standard-game":
                return (
                  <span>
                    The game ends once any team has reached 1000 points.
                  </span>
                );
              case "single-round":
                return <span>The game ends after a single round.</span>;
            }
          })()}{" "}
          The team with the highest score wins.
        </p>

        <p>
          {(() => {
            switch (props.settings.tiebreaker) {
              case "no-tiebreaker":
                return (
                  <span>In case of a tie, the game will end in a draw.</span>
                );
              case "play-another-round":
                return (
                  <span>
                    In case of a tie, play will continue until a winner is
                    determined.
                  </span>
                );
            }
          })()}
        </p>
      </details>
    </section>
  );
}

const definitionListCss = css`
  dt {
    font-weight: 500;
  }

  dd {
    margin: 0;
    :not(:last-of-type) {
      margin-bottom: 1em;
    }

    > p {
      margin: 0;
      :not(:last-of-type) {
        margin-bottom: 0.5em;
      }
    }
  }
`;
