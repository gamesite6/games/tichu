import {
  ActionHandler,
  Card,
  classifyCombo,
  comboValue,
  dragon,
  Following,
  Player,
  GameState,
  Leading,
  mustFulfillWish,
  fulfillsWish,
} from "../shared";
import * as React from "react";
import { ClientState, PlayingState } from "../clientState";

type Props = {
  state: GameState;
  phase: Leading | Following;
  user: Player;
  clientState: PlayingState;
  onAction: ActionHandler;
  sortedPlayers: Player[];
};
export function ActionButtons({
  state,
  phase,
  clientState,
  user,
  onAction,
  sortedPlayers,
}: Props) {
  const mustFulfill =
    state.wish !== undefined && mustFulfillWish(phase, user, state.wish);
  const fulfills =
    state.wish === undefined || fulfillsWish(state.wish, clientState.selected);

  if (phase.kind === "Leading") {
    const combo = classifyCombo(clientState.selected);
    const mayBet = user.hand.length === 14 && !user.bet;

    const showPlayButton = phase.player === user.id;
    const enablePlayButton =
      showPlayButton && combo && (!mustFulfill || fulfills);

    return (
      <>
        {mayBet ? (
          <button
            className="secondary"
            onClick={() =>
              onAction({
                kind: "BetSmall",
              })
            }
          >
            Bet 100 points
          </button>
        ) : null}{" "}
        {showPlayButton ? (
          <button
            className="primary"
            disabled={!enablePlayButton}
            onClick={() =>
              onAction({
                kind: "Play",
                cards: clientState.selected,
                wish: clientState.wish,
              })
            }
          >
            Play cards!
          </button>
        ) : null}
      </>
    );
  } else if (phase.kind === "Following") {
    const mayBet = user.hand.length === 14 && !user.bet;

    const previousCards: Card[] = phase.cards[phase.cards.length - 1];
    const previousCombo = classifyCombo(previousCards);

    const selectedCards = clientState.selected;
    const selectedCombo = classifyCombo(selectedCards);
    const selectedComboValue =
      selectedCombo && comboValue(selectedCombo, selectedCards, phase.value);

    const showBombButton = user.id !== phase.player && user.hand.length !== 0;

    const enableBombButton =
      selectedCombo === "bomb" &&
      selectedComboValue !== null &&
      selectedComboValue > phase.value;

    const showPassButton = user.id === phase.player && user.id !== phase.leader;
    const enablePassButton =
      !state.wish || !mustFulfillWish(phase, user, state.wish);

    const showPlayButton = user.id === phase.player && user.id !== phase.leader;

    const enablePlayButton =
      selectedCombo !== null &&
      selectedComboValue !== null &&
      ((selectedCombo === previousCombo &&
        selectedCards.length === previousCards.length) ||
        selectedCombo === "bomb") &&
      selectedComboValue > phase.value &&
      (!mustFulfill || fulfills);

    const showClaimButton =
      user.id === phase.player && user.id === phase.leader;

    return (
      <>
        {mayBet ? (
          <button
            className="secondary"
            onClick={() =>
              onAction({
                kind: "BetSmall",
              })
            }
          >
            Bet 100 points
          </button>
        ) : null}{" "}
        {showPassButton ? (
          <button
            className="secondary"
            disabled={!enablePassButton}
            onClick={() => onAction({ kind: "Pass" })}
          >
            Pass
          </button>
        ) : null}{" "}
        {showPlayButton ? (
          <button
            className="primary"
            disabled={!enablePlayButton}
            onClick={() => onAction({ kind: "Play", cards: selectedCards })}
          >
            Play cards
          </button>
        ) : null}{" "}
        {showBombButton ? (
          <button
            className="primary"
            disabled={!enableBombButton}
            onClick={() => onAction({ kind: "Bomb", cards: selectedCards })}
          >
            Play bomb!
          </button>
        ) : null}{" "}
        {showClaimButton ? (
          <ClaimButton
            players={sortedPlayers}
            onAction={onAction}
            phase={phase}
          />
        ) : null}
      </>
    );
  }

  return null;
}

function ClaimButton(props: {
  players: Player[];
  onAction: ActionHandler;
  phase: Following;
}) {
  const { onAction, phase, players } = props;
  const winningCombo = phase.cards[phase.cards.length - 1];
  const isSingleDragon =
    classifyCombo(winningCombo) === "single" && winningCombo[0] === dragon;

  if (isSingleDragon) {
    const leftPlayer = players[1];
    const rightPlayer = players[3];
    return (
      <>
        <button
          className="primary"
          onClick={() => onAction({ kind: "Claim", giveTo: leftPlayer.id })}
        >
          Give cards to <br />
          <gs6-user playerid={leftPlayer.id} />
        </button>{" "}
        <button
          className="primary"
          onClick={() => onAction({ kind: "Claim", giveTo: rightPlayer.id })}
        >
          Give cards to <br />
          <gs6-user playerid={rightPlayer.id} />
        </button>
      </>
    );
  } else {
    return (
      <button className="primary" onClick={() => onAction({ kind: "Claim" })}>
        Claim cards
      </button>
    );
  }
}
