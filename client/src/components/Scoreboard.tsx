import { Team } from "../shared";
import * as React from "react";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { formatPoints } from "../util";

type HtmlAttrs = React.HTMLAttributes<HTMLElement>;
type Props = { teams: [Team, Team] };
export function Scoreboard(props: Props & HtmlAttrs) {
  const { teams, ...htmlAttrs } = props;

  return (
    <section {...htmlAttrs}>
      <table
        css={css`
          border-collapse: collapse;
          text-align: center;
        `}
      >
        <thead
          css={`
            th {
              border-bottom: solid gray 2px;
            }
          `}
        >
          <tr>
            <th
              css={css`
                width: 5em;
              `}
            >
              Team
            </th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody
          css={css`
            td {
              padding-top: 0.4em;
              padding-bottom: 0.4em;
              border-top: solid lightgray 1px;
            }
          `}
        >
          {teams.map((team) => (
            <tr key={team.members.map((p) => p.id).join("-")}>
              <td
                css={css`
                  text-align: center;
                `}
              >
                {team.members.map((member) => (
                  <span key={member.id}>
                    <gs6-user compact playerid={member.id} />
                  </span>
                ))}
              </td>
              <td
                css={css`
                  font-weight: 500;
                `}
              >
                {formatPoints(team.score)}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </section>
  );
}
