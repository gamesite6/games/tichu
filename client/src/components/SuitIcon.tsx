import { Card, getSuit, Suit, SuitedCard } from "../shared";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";

function getSuitEmoji(suit: Suit): String {
  switch (suit) {
    case "gems":
      return "💎";
    case "castles":
      return "🏯";
    case "stars":
      return "⭐";
    case "swords":
      return "🗡️";
  }
}

function suitIconCss(suit: Suit) {
  switch (suit) {
    case "gems":
      return css`
        filter: grayscale(1) contrast(0.5) sepia(1) hue-rotate(80deg)
          saturate(2) brightness(0.9);
      `;
    case "castles":
      return css`
        filter: grayscale(1) contrast(0.5) sepia(1) hue-rotate(181deg)
          saturate(2) brightness(0.9);
      `;
    case "swords":
      return css`
        filter: grayscale(1) brightness(0.5);
      `;
    case "stars":
      return css`
        filter: grayscale(1) contrast(0.1) sepia(1) saturate(10)
          hue-rotate(318deg) brightness(0.9);
      `;
  }
}

type Props =
  | { suit: Suit; card?: undefined }
  | { suit?: undefined; card: SuitedCard };
export function SuitIcon(props: Props) {
  const suit = props.suit ?? getSuit(props.card);
  return (
    <span className="emoji" css={suitIconCss(suit)}>
      {getSuitEmoji(suit)}
    </span>
  );
}
