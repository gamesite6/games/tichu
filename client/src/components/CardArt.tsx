import {
  Card,
  dog,
  dragon,
  getRank,
  getSuit,
  isSuited,
  phoenix,
  sparrow,
  SpecialCard,
  Suit,
  SuitedCard,
} from "../shared";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { SuitIcon } from "./SuitIcon";
import { ReactElement } from "react";
import { getSpecialCardValueStyle } from "../util";

type Props = {
  card: Card;
};

export function CardArt({ card }: Props) {
  return (
    <div
      css={css`
        height: 100%;
        font-size: 150%;

        .flipped {
          transform: rotate(180deg);
        }
      `}
    >
      {(() => {
        if (isSuited(card)) {
          const rank = getRank(card);
          switch (rank) {
            case 2:
              return <Two card={card} />;
            case 3:
              return <Three card={card} />;
            case 4:
              return <Four card={card} />;
            case 5:
              return <Five card={card} />;
            case 6:
              return <Six card={card} />;
            case 7:
              return <Seven card={card} />;
            case 8:
              return <Eight card={card} />;
            case 9:
              return <Nine card={card} />;
            case 10:
              return <Ten card={card} />;

            case 11:
            case 12:
            case 13:
              return <FaceCard rank={rank} suit={getSuit(card)} />;

            case 14:
              return <Ace card={card} />;
            default:
              return null;
          }
        } else {
          return <Special card={card} />;
        }
      })()}
    </div>
  );
}

type SuitedProps = {
  card: SuitedCard;
};

type SpecialProps = {
  card: SpecialCard;
};

function getSpecialCardArtEmoji(card: SpecialCard): string {
  switch (card) {
    case dog:
      return "🐕";
    case dragon:
      return "🐉";
    case sparrow:
      return "🐦";
    case phoenix:
      return "🦚";
  }
}

function flip(el: ReactElement) {
  return <div className="flipped">{el}</div>;
}

function Special({ card }: SpecialProps) {
  return (
    <div
      className="emoji"
      css={css`
        ${getSpecialCardValueStyle({ card })}

        height: 100%;
        display: grid;
        font-size: 300%;
        place-content: center;
      `}
    >
      {getSpecialCardArtEmoji(card)}
    </div>
  );
}

const columnCss = css`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

function Ace({ card }: SuitedProps) {
  return (
    <div
      css={css`
        height: 100%;
        display: grid;
        font-size: 200%;
        place-content: center;
      `}
    >
      <SuitIcon card={card} />
    </div>
  );
}

function Two({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div css={columnCss}>
      {Icon}
      {flip(Icon)}
    </div>
  );
}

function Three({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div css={columnCss}>
      {Icon}
      {Icon}
      {flip(Icon)}
    </div>
  );
}

function Four({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        ${columnCss}

        flex-wrap: wrap;

        > * {
          height: 50%;
          display: grid;
          place-content: center;
        }
      `}
    >
      {Icon}
      {flip(Icon)}

      {Icon}
      {flip(Icon)}
    </div>
  );
}

function Five({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        position: relative;
        height: 100%;
      `}
    >
      <Four card={card} />

      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 50%;
          transform: translate(-50%, -50%);
        `}
      >
        {Icon}
      </div>
    </div>
  );
}

function Six({ card }: SuitedProps) {
  return (
    <div
      css={css`
        height: 100%;
        display: flex;
        justify-content: space-around;
      `}
    >
      <Three card={card} />
      <Three card={card} />
    </div>
  );
}

function Seven({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        position: relative;
        height: 100%;
      `}
    >
      <Six card={card} />

      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 33%;
          transform: translate(-50%, -50%);
        `}
      >
        {Icon}
      </div>
    </div>
  );
}

function Eight({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        position: relative;
        height: 100%;
      `}
    >
      <Six card={card} />

      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 33.33%;
          transform: translate(-50%, -50%);
        `}
      >
        {Icon}
      </div>
      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 66.67%;
          transform: translate(-50%, -50%) rotate(180deg);
        `}
      >
        {Icon}
      </div>
    </div>
  );
}

function Nine({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        height: 100%;
        position: relative;
        display: flex;
        justify-content: space-around;
      `}
    >
      <div css={columnCss}>
        {Icon}
        {Icon}
        {flip(Icon)}
        {flip(Icon)}
      </div>

      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 25%;
          transform: translate(-50%, -50%);
        `}
      >
        {Icon}
      </div>

      <div css={columnCss}>
        {Icon}
        {Icon}
        {flip(Icon)}
        {flip(Icon)}
      </div>
    </div>
  );
}

function Ten({ card }: SuitedProps) {
  const Icon = <SuitIcon card={card} />;
  return (
    <div
      css={css`
        height: 100%;
        position: relative;
        display: flex;
        justify-content: space-around;
      `}
    >
      <div css={columnCss}>
        {Icon}
        {Icon}
        {flip(Icon)}
        {flip(Icon)}
      </div>

      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 25%;
          transform: translate(-50%, -50%);
        `}
      >
        {Icon}
      </div>
      <div
        css={css`
          position: absolute;
          left: 50%;
          top: 75%;
          transform: translate(-50%, -50%) rotate(180deg);
        `}
      >
        {Icon}
      </div>

      <div css={columnCss}>
        {Icon}
        {Icon}
        {flip(Icon)}
        {flip(Icon)}
      </div>
    </div>
  );
}

function getFaceCardEmoji(rank: 11 | 12 | 13) {
  switch (rank) {
    case 11:
      return "👲🏼";
    case 12:
      return "👸🏼";
    case 13:
      return "🤴🏼";
  }
}

export function suitIconCss(suit: Suit) {
  switch (suit) {
    case "gems":
      return css`
        filter: sepia(1) hue-rotate(80deg) brightness(0.9);
      `;
    case "castles":
      return css`
        filter: sepia(1) saturate(3) hue-rotate(181deg) brightness(0.9);
      `;
    case "swords":
      return css`
        filter: grayscale(1) brightness(0.8);
      `;
    case "stars":
      return css`
        filter: sepia(1) saturate(2) hue-rotate(318deg) brightness(0.9);
      `;
  }
}

function FaceCard({ rank, suit }: { rank: 11 | 12 | 13; suit: Suit }) {
  return (
    <div
      className="emoji"
      css={css`
        font-size: 300%;
        height: 100%;
        display: grid;
        place-content: center;

        ${suitIconCss(suit)}
      `}
    >
      {getFaceCardEmoji(rank)}
    </div>
  );
}
