import {
  Card,
  dragon,
  getRank,
  getSuit,
  dog,
  isSuited,
  sparrow,
  phoenix,
  SpecialCard,
  Suit,
  SuitedCard,
} from "../shared";
import { CardArt } from "./CardArt";
import { SuitIcon } from "./SuitIcon";
import { getRankText, getSpecialCardValueStyle } from "../util";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { motion } from "framer-motion";

function rankColor({ suit }: { suit: Suit }): string {
  switch (suit) {
    case "gems":
      return "#078e50";
    case "castles":
      return "#3065d0";
    case "swords":
      return "#2f2f2f";
    case "stars":
      return "#f53d40";
  }
}

function RankC(props: { suit: Suit; rank: number }) {
  return (
    <div
      css={css`
        font-size: 2em;
        line-height: 1em;
        color: ${rankColor(props)};
      `}
    >
      {getRankText(props.rank)}
    </div>
  );
}

function SuitedCardValue(props: { card: SuitedCard }) {
  const suit = getSuit(props.card);
  const rank = getRank(props.card);
  return (
    <div
      css={css`
        position: absolute;
        width: 21.5%;
        left: 0;
        top: 0.25em;
        text-align: center;
      `}
    >
      <RankC suit={suit} rank={rank} />
      <SuitIcon suit={suit} />
    </div>
  );
}

function getSpecialCardEmoji(card: SpecialCard): string {
  switch (card) {
    case dragon:
      return "🐲";
    case phoenix:
      return "🦚";
    case dog:
      return "🐶";
    case sparrow:
      return "🐦";
  }
}

export function SpecialCardIcon(props: { card: SpecialCard }) {
  return (
    <span className="emoji" css={getSpecialCardValueStyle(props)}>
      {getSpecialCardEmoji(props.card)}
    </span>
  );
}

function SpecialCardValue(props: { card: SpecialCard }) {
  return (
    <div
      css={css`
        position: absolute;
        left: 1%;
        top: 5%;
        font-size: 1.5em;
        line-height: 1em;
      `}
    >
      <SpecialCardIcon card={props.card} />
    </div>
  );
}

const cardInlineCss = css`
  display: inline-block;
  font-family: "Caveat Brush", cursive;
  background-color: linen;
  width: calc(10em / 6);
  height: calc(14em / 6);
`;
export function CardFaceInline(props: { card: Card }) {
  return (
    <motion.div
      className="border"
      css={cardInlineCss}
      layoutId={props.card.toString()}
      transition={{ ease: "easeInOut", duration: 0.25 }}
    >
      <div
        css={css`
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          text-align: center;
        `}
      >
        {(() => {
          if (isSuited(props.card)) {
            const rank = getRank(props.card);
            const suit = getSuit(props.card);
            return (
              <div
                css={css`
                  font-size: 67%;
                `}
              >
                <RankC suit={suit} rank={rank} />
                <SuitIcon suit={suit} />
              </div>
            );
          } else {
            return (
              <div>
                <SpecialCardIcon card={props.card} />
              </div>
            );
          }
        })()}
      </div>
    </motion.div>
  );
}

const cardStyle = css`
  font-size: inherit;
  font-family: "Caveat Brush", cursive;
  text-align: center;
  box-sizing: border-box;
  background-color: linen;
  position: relative;
  width: 10em;
  height: 14em;
  border-radius: 7% / 5%;
  padding: 0.75em;
`;

export function CardFace(props: { card: Card }) {
  return (
    <motion.div
      className="border"
      css={cardStyle}
      layoutId={props.card.toString()}
      transition={{ ease: "easeInOut", duration: 0.25 }}
    >
      {(() => {
        if (isSuited(props.card)) return <SuitedCardValue card={props.card} />;
        else return <SpecialCardValue card={props.card} />;
      })()}
      <div
        className="card-art"
        css={css`
          display: relative;

          border-radius: 0.25em;
          transform: translateY(-0.75em);
          height: calc(100% + 1.5em);
          width: 67%;
          margin: auto;
        `}
      >
        <CardArt card={props.card} />
      </div>
    </motion.div>
  );
}

export function CardBack(props: { card: Card }) {
  return (
    <motion.div
      css={cardStyle}
      className="border"
      layoutId={props.card.toString()}
      transition={{ ease: "easeInOut", duration: 0.25 }}
    >
      <div
        css={css`
          box-sizing: border-box;
          position: absolute;
          top: 0.75em;
          bottom: 0.75em;
          left: 0.75em;
          right: 0.75em;
          border-radius: 4.1667% / 2.8333%;
          background: repeating-linear-gradient(
              45deg,
              #00000010,
              #00000010 5%,
              #00000020 5%,
              #00000020 10%
            ),
            repeating-linear-gradient(
              -45deg,
              #00000010,
              #00000010 5%,
              #00000020 5%,
              #00000020 10%
            ),
            radial-gradient(hsl(6deg 70% 45%), hsl(6deg 50% 45%));
        `}
      />
    </motion.div>
  );
}

export function CardEmpty() {
  return (
    <div
      css={css`
        border: dotted #00000075 0.25em;
        box-sizing: border-box;
        width: 10em;
        height: 14em;
        border-radius: 7% / 5%;
        padding: 0.75em;
        background: transparent;
      `}
    />
  );
}
