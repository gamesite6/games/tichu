import { Card, getScoreValue, Phase, Player } from "../shared";
import { sum } from "lodash";
import { sortCards } from "../util";
import { CardBack, CardFace, CardFaceInline } from "./Card";
import * as React from "react";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react";
import { sortedAscending } from "../shared/util";
import { motion } from "framer-motion";

type CardFanLiProps = { index: number; size: number; isUser: boolean };

function cardFanItemCss({ index, size, isUser }: CardFanLiProps) {
  const angleStep = 5;
  const angle = ((-size + 1) / 2 + index) * angleStep;
  return css`
    font-size: ${isUser ? "0.9em" : "0.5em"};
    position: absolute;
    top: 50%;
    left: 50%;
    transform-origin: center 200%;
    transform: translate(-50%, -100%) rotateZ(${angle}deg);
  `;
}

function cardButtonCss(highlighted: boolean) {
  return css`
    transition: transform 0.2s;
    :not(:disabled) {
      cursor: pointer;

      :hover,
      :focus {
        transform: translateY(-5%);
      }
    }
    ${highlighted
      ? css`
          transform: translateY(-20%) !important;
        `
      : ""}
  `;
}

export function PlayerC({
  player,
  playerIndex,
  isUser,
  thinking,
  onCardClick,
  speech,
  children,
  hideCards,
  highlightCards,
  revealCardFaces,
}: {
  player: Player;
  playerIndex: number;
  isUser: boolean;
  thinking: boolean;
  onCardClick?: (card: Card) => any;
  speech?: any;
  children?: any;

  hideCards: Card[];
  highlightCards: Card[];

  revealCardFaces: boolean;
}) {
  const playerBoxProps: { thinking?: boolean; speaking?: boolean } = {};
  if (thinking) {
    playerBoxProps.thinking = true;
  }
  if (speech) {
    playerBoxProps.speaking = true;
  }

  const cards = [...player.hand].filter((card) => !hideCards.includes(card));

  return (
    <div
      css={css`
        position: relative;
        max-width: 14em;
      `}
    >
      <ul
        css={css`
          position: absolute;
          left: 50%;
        `}
        title={!isUser ? `${cards.length} cards remaining` : undefined}
      >
        {sortCards(cards).map((card, index) => (
          <li
            css={cardFanItemCss({ index, size: cards.length, isUser })}
            key={card}
          >
            {isUser || revealCardFaces ? (
              <button
                css={cardButtonCss(highlightCards.includes(card))}
                className="invisibutton"
                disabled={!onCardClick}
                onClick={() => onCardClick?.(card)}
              >
                <CardFace card={card} />
              </button>
            ) : (
              <CardBack card={card} />
            )}
          </li>
        ))}
      </ul>
      <gs6-player-box key={player.id} playerid={player.id} {...playerBoxProps}>
        <div
          css={css`
            text-align: center;
          `}
        >
          Points:{" "}
          <strong
            css={css`
              font-weight: 500;
            `}
          >
            {sum(player.claimed.map(getScoreValue))}
          </strong>
        </div>
        {children}
        <div slot="speech">{speech}</div>

        <div className="pointless-cards">
          {player.claimed
            .filter((card) => getScoreValue(card) === 0)
            .map((card, index) => (
              <motion.div
                key={card}
                css={css`
                  position: absolute;
                  font-size: 20%;
                  left: 50%;
                  transform: translateX(-50%);
                `}
                animate={{ opacity: 0 }}
              >
                <CardBack card={card} />
              </motion.div>
            ))}
        </div>

        <div
          className="scoring-cards"
          css={css`
            position: absolute;
            bottom: 0;
            left: 50%;
            transform: translate(-50%, 75%);

            display: flex;

            > div {
              margin: 0 -0.2em;
            }
          `}
        >
          {sortedAscending(player.claimed)
            .filter((card) => getScoreValue(card) !== 0)
            .map((card, index) => (
              <div key={card} css={css``}>
                <CardFaceInline card={card} />
              </div>
            ))}
        </div>
      </gs6-player-box>
    </div>
  );
}
