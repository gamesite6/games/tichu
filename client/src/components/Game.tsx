import {
  Action,
  Card,
  getTeams,
  PlayerId,
  GameSettings,
  GameState,
  Bet,
  sparrow,
} from "../shared";
import produce from "immer";
import { remove } from "lodash";
import * as React from "react";
import { useState } from "react";

import {
  cardIntersection,
  ClientState,
  getInitialClientState,
} from "../clientState";
import { playerIsThinking, sortPlayers } from "../util";
import { ActionButtons } from "./ActionButtons";
import { Center } from "./Center";
import { PlayerC } from "./Player";
import { RoundSummary } from "./RoundSummary";
import { Scoreboard } from "./Scoreboard";

/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx, SerializedStyles } from "@emotion/react";
import { SpecialCardIcon } from "./Card";

const globalGameCss = css`
  em {
    font-weight: 300;
  }
`;

const scoreboardWrapperCss = (props: { expanded: boolean }) => css`
  position: absolute;

  top: 50%;
  left: 50%;
  margin-top: 2.5em;
  ${getScoreboardSize(props)}

  transition: transform 0.4s, top 0.4s, left 0.4s, height 0.4s, min-height 0.4s,
    width 0.4s, min-width 0.4s;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function getScoreboardSize(props: { expanded: boolean }) {
  if (props.expanded) {
    return css`
      width: 30em;
      height: 32em;
      transform: translate(-50%, -50%);
    `;
  } else {
    return css`
      width: 9em;
      height: 8em;
      transform: translate(0, -100%) translateX(17em) translateY(-19em);
    `;
  }
}

function getPlayerTransforms(playerIndex: number): SerializedStyles | string {
  switch (playerIndex) {
    case 0:
      return css`
        transform: translate(-50%, 0) translateY(19em);
      `;
    case 1:
      return css`
        transform: translate(-100%, -50%) translateX(-17em);
      `;
    case 2:
      return css`
        transform: translate(-50%, -100%) translateY(-19em);
      `;
    case 3:
      return css`
        transform: translate(0, -50%) translateX(17em);
      `;

    default:
      return "";
  }
}

const playerWrapperCss = (props: { index: number }) => css`
  position: absolute;

  margin-top: 2.5em;
  top: 50%;
  left: 50%;
  ${getPlayerTransforms(props.index)}
`;

type Props = {
  userId: PlayerId | undefined;
  settings: GameSettings;
  state: GameState;
  onAction: (action: Action) => void;
};
export default function Game(props: Props) {
  const sortedPlayers = sortPlayers(props.userId, props.state.players);

  const user = props.state.players.find((p) => p.id === props.userId);
  const [clientState, setClientState] = useState<ClientState>(
    getInitialClientState(props.state, user)
  );

  const newClientState = getInitialClientState(props.state, user);
  if (
    newClientState?.kind !== clientState?.kind ||
    newClientState?.userId !== clientState?.userId
  ) {
    setClientState(newClientState);
  } else if (user) {
    const fixedCardsClientState = cardIntersection(clientState, user.hand);
    if (fixedCardsClientState) {
      setClientState(fixedCardsClientState);
    }
  }

  let handleCardClick: ((card: Card) => void) | null;
  if (user) {
    handleCardClick = (() => {
      switch (clientState?.kind) {
        case "Pushing":
          return (card: Card) => {
            const selectedPlayerId = clientState.selected;
            if (selectedPlayerId) {
              const nextClientState = produce(clientState, (draft) => {
                draft.pushed[selectedPlayerId] = card;
                const nextSelectedPlayerId = sortedPlayers.find(
                  (player) =>
                    player.id !== user.id &&
                    player.id !== selectedPlayerId &&
                    !clientState.pushed[player.id]
                );
                draft.selected = nextSelectedPlayerId?.id;
              });

              setClientState(nextClientState);
            }
          };
        case "Playing": {
          return (clickedCard: Card) => {
            const nextClientState = produce(clientState, (draft) => {
              const removedCards = remove(
                draft.selected,
                (c) => c === clickedCard
              );
              if (removedCards[0] === undefined) {
                draft.selected.push(clickedCard);
              }
            });
            setClientState(nextClientState);
          };
        }
        case "Empty":
          return null;
      }
    })();
  }

  return (
    <div css={globalGameCss}>
      <div
        css={css`
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        `}
      >
        <Center
          state={props.state}
          sortedPlayers={sortedPlayers}
          user={user}
          onAction={props.onAction}
          clientState={clientState}
          setClientState={setClientState}
        />
      </div>
      {sortedPlayers.map((player, index) => {
        const isUser = player.id === props.userId;
        return (
          <div key={player.id} css={playerWrapperCss({ index })}>
            <PlayerC
              player={player}
              playerIndex={index}
              isUser={isUser}
              thinking={playerIsThinking(props.state, player.id)}
              onCardClick={handleCardClick || undefined}
              hideCards={getCardsToHide(player.id, props.state, clientState)}
              highlightCards={isUser ? getCardsToHighlight(clientState) : []}
              revealCardFaces={
                props.state.phase.kind === "RoundComplete" ||
                props.state.phase.kind === "GameComplete"
              }
            >
              {player.bet && (
                <div
                  css={css`
                    text-align: center;
                  `}
                >
                  {BetIndicator(player.bet)}
                </div>
              )}
            </PlayerC>

            {isUser &&
            clientState.kind === "Playing" &&
            clientState.selected.includes(sparrow) ? (
              <div
                className="panel"
                css={css`
                  position: absolute;
                  top: 0;
                  left: 50%;

                  transform: translate(-50%, -100%) translateY(-15rem);
                `}
              >
                <label>
                  <SpecialCardIcon card={sparrow} /> Wish for rank{" "}
                  <select
                    css={css`
                      height: 1.75em;
                      padding: 0;
                    `}
                    onChange={(evt) => {
                      const rank = parseInt(evt.target.value);

                      setClientState({
                        ...clientState,
                        wish: Number.isInteger(rank) ? rank : undefined,
                      });
                    }}
                  >
                    <option value="">None</option>
                    <option value={14}>A</option>
                    <option value={13}>K</option>
                    <option value={12}>Q</option>
                    <option value={11}>J</option>
                    {[10, 9, 8, 7, 6, 5, 4, 3, 2].map((rank: number) => (
                      <option value={rank}>{rank}</option>
                    ))}
                  </select>
                </label>

                <div
                  css={css`
                    margin-top: 0.25rem;
                    min-width: 20em;
                    line-height: 1;
                    filter: opacity(80%);
                  `}
                >
                  <small>
                    The next player who is able to play a combination with a
                    card of this rank during their turn, must do so.
                  </small>
                </div>
              </div>
            ) : null}

            {isUser &&
            (props.state.phase.kind === "Leading" ||
              props.state.phase.kind === "Following") &&
            clientState.kind === "Playing" ? (
              <div css={actionButtonsCss}>
                <ActionButtons
                  state={props.state}
                  phase={props.state.phase}
                  user={player}
                  clientState={clientState}
                  onAction={props.onAction}
                  sortedPlayers={sortedPlayers}
                />
              </div>
            ) : null}
          </div>
        );
      })}
      <div
        css={scoreboardWrapperCss({
          expanded: props.state.phase.kind === "RoundComplete",
        })}
        className="panel border shadow"
      >
        {props.state.phase.kind === "RoundComplete" ? (
          <RoundSummary
            teams={getTeams(props.state)}
            phase={props.state.phase}
            onAction={props.onAction}
            user={user}
          />
        ) : (
          <Scoreboard teams={getTeams(props.state)} />
        )}
      </div>
    </div>
  );
}

const actionButtonsCss = css`
  position: absolute;
  left: 50%;
  top: -0.5em;
  transform: translate(-50%, -100%);
  white-space: nowrap;
`;

function BetIndicator(bet: Bet) {
  switch (bet) {
    case 100:
      return (
        <em>
          <i className="emoji">🐶</i> Bet 100
        </em>
      );
    case 200:
      return (
        <em>
          <i className="emoji">🐲</i> Bet 200
        </em>
      );
  }
}

function getCardsToHide(
  playerId: PlayerId,
  state: GameState,
  clientState: ClientState
): Card[] {
  switch (state.phase.kind) {
    case "Pushing": {
      if (clientState?.kind === "Pushing") {
        const pushed = state.phase.pushed[playerId] || clientState.pushed;
        return Object.values(pushed);
      } else {
        return [];
      }
    }

    case "BigBetting":
    case "Receiving":
    case "Leading":
    case "Following":
    case "RoundComplete":
    case "GameComplete": {
      return [];
    }
  }
}

function getCardsToHighlight(clientState: ClientState): Card[] {
  switch (clientState?.kind) {
    case "Pushing":
      return [];
    case "Playing":
      return clientState.selected;
    case "Empty":
      return [];
  }
}
