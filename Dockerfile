FROM node:16.13.0-alpine as build

RUN npm install --global pnpm

WORKDIR /workspace

COPY . .

RUN pnpm install

RUN (cd server && pnpm run build && pnpm prune --prod)

FROM node:16.13.0-alpine

WORKDIR /app
COPY --from=build /workspace .

WORKDIR /app/server
ENTRYPOINT [ "node", "." ]
