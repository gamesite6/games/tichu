export type Suit = Gems | Swords | Castles | Stars;

export type Gems = "gems";
export type Swords = "swords";
export type Castles = "castles";
export type Stars = "stars";

export type Card = SpecialCard | SuitedCard;

export type SpecialCard = Dog | Sparrow | Phoenix | Dragon;

export type SuitedCard =
  | Gems2
  | Gems3
  | Gems4
  | Gems5
  | Gems6
  | Gems7
  | Gems8
  | Gems9
  | Gems10
  | GemsJ
  | GemsQ
  | GemsK
  | GemsA
  | Swords2
  | Swords3
  | Swords4
  | Swords5
  | Swords6
  | Swords7
  | Swords8
  | Swords9
  | Swords10
  | SwordsJ
  | SwordsQ
  | SwordsK
  | SwordsA
  | Castles2
  | Castles3
  | Castles4
  | Castles5
  | Castles6
  | Castles7
  | Castles8
  | Castles9
  | Castles10
  | CastlesJ
  | CastlesQ
  | CastlesK
  | CastlesA
  | Stars2
  | Stars3
  | Stars4
  | Stars5
  | Stars6
  | Stars7
  | Stars8
  | Stars9
  | Stars10
  | StarsJ
  | StarsQ
  | StarsK
  | StarsA;

export type Dog = -1;
export type Sparrow = 1;
export type Phoenix = 54;
export type Dragon = 55;

export const dog: Dog = -1;
export const sparrow: Sparrow = 1;
export const phoenix: Phoenix = 54;
export const dragon: Dragon = 55;

export type Gems2 = 2;
export const gems2: Gems2 = 2;
export type Swords2 = 3;
export const swords2: Swords2 = 3;
export type Castles2 = 4;
export const castles2: Castles2 = 4;
export type Stars2 = 5;
export const stars2: Stars2 = 5;
export type Gems3 = 6;
export const gems3: Gems3 = 6;
export type Swords3 = 7;
export const swords3: Swords3 = 7;
export type Castles3 = 8;
export const castles3: Castles3 = 8;
export type Stars3 = 9;
export const stars3: Stars3 = 9;
export type Gems4 = 10;
export const gems4: Gems4 = 10;
export type Swords4 = 11;
export const swords4: Swords4 = 11;
export type Castles4 = 12;
export const castles4: Castles4 = 12;
export type Stars4 = 13;
export const stars4: Stars4 = 13;
export type Gems5 = 14;
export const gems5: Gems5 = 14;
export type Swords5 = 15;
export const swords5: Swords5 = 15;
export type Castles5 = 16;
export const castles5: Castles5 = 16;
export type Stars5 = 17;
export const stars5: Stars5 = 17;
export type Gems6 = 18;
export const gems6: Gems6 = 18;
export type Swords6 = 19;
export const swords6: Swords6 = 19;
export type Castles6 = 20;
export const castles6: Castles6 = 20;
export type Stars6 = 21;
export const stars6: Stars6 = 21;
export type Gems7 = 22;
export const gems7: Gems7 = 22;
export type Swords7 = 23;
export const swords7: Swords7 = 23;
export type Castles7 = 24;
export const castles7: Castles7 = 24;
export type Stars7 = 25;
export const stars7: Stars7 = 25;
export type Gems8 = 26;
export const gems8: Gems8 = 26;
export type Swords8 = 27;
export const swords8: Swords8 = 27;
export type Castles8 = 28;
export const castles8: Castles8 = 28;
export type Stars8 = 29;
export const stars8: Stars8 = 29;
export type Gems9 = 30;
export const gems9: Gems9 = 30;
export type Swords9 = 31;
export const swords9: Swords9 = 31;
export type Castles9 = 32;
export const castles9: Castles9 = 32;
export type Stars9 = 33;
export const stars9: Stars9 = 33;
export type Gems10 = 34;
export const gems10: Gems10 = 34;
export type Swords10 = 35;
export const swords10: Swords10 = 35;
export type Castles10 = 36;
export const castles10: Castles10 = 36;
export type Stars10 = 37;
export const stars10: Stars10 = 37;
export type GemsJ = 38;
export const gemsJ: GemsJ = 38;
export type SwordsJ = 39;
export const swordsJ: SwordsJ = 39;
export type CastlesJ = 40;
export const castlesJ: CastlesJ = 40;
export type StarsJ = 41;
export const starsJ: StarsJ = 41;
export type GemsQ = 42;
export const gemsQ: GemsQ = 42;
export type SwordsQ = 43;
export const swordsQ: SwordsQ = 43;
export type CastlesQ = 44;
export const castlesQ: CastlesQ = 44;
export type StarsQ = 45;
export const starsQ: StarsQ = 45;
export type GemsK = 46;
export const gemsK: GemsK = 46;
export type SwordsK = 47;
export const swordsK: SwordsK = 47;
export type CastlesK = 48;
export const castlesK: CastlesK = 48;
export type StarsK = 49;
export const starsK: StarsK = 49;
export type GemsA = 50;
export const gemsA: GemsA = 50;
export type SwordsA = 51;
export const swordsA: SwordsA = 51;
export type CastlesA = 52;
export const castlesA: CastlesA = 52;
export type StarsA = 53;
export const starsA: StarsA = 53;

export const cards: Card[] = [
  dog,
  sparrow,
  gems2,
  gems3,
  gems4,
  gems5,
  gems6,
  gems7,
  gems8,
  gems9,
  gems10,
  gemsJ,
  gemsQ,
  gemsK,
  gemsA,
  swords2,
  swords3,
  swords4,
  swords5,
  swords6,
  swords7,
  swords8,
  swords9,
  swords10,
  swordsJ,
  swordsQ,
  swordsK,
  swordsA,
  castles2,
  castles3,
  castles4,
  castles5,
  castles6,
  castles7,
  castles8,
  castles9,
  castles10,
  castlesJ,
  castlesQ,
  castlesK,
  castlesA,
  stars2,
  stars3,
  stars4,
  stars5,
  stars6,
  stars7,
  stars8,
  stars9,
  stars10,
  starsJ,
  starsQ,
  starsK,
  starsA,
  phoenix,
  dragon,
];

Object.freeze(cards);
