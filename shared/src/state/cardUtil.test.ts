import { isSpecial, getRank, getSuit } from "./cardUtil";
import * as c from "./card";

describe("cardUtil", () => {
  describe("isSpecial", () => {
    test("dog, sparrow, phoenix and dragon", () => {
      expect(isSpecial(c.dog)).toBe(true);
      expect(isSpecial(c.sparrow)).toBe(true);
      expect(isSpecial(c.phoenix)).toBe(true);
      expect(isSpecial(c.dragon)).toBe(true);
    });
  });

  describe("getRank", () => {
    test("mah jong has rank 1", () => {
      expect(getRank(c.sparrow)).toBe(1);
    });
    test("number cards have their number as rank", () => {
      expect(getRank(c.gems2)).toBe(2);
      expect(getRank(c.swords3)).toBe(3);
      expect(getRank(c.castles4)).toBe(4);
      expect(getRank(c.stars5)).toBe(5);
      expect(getRank(c.gems6)).toBe(6);
      expect(getRank(c.swords7)).toBe(7);
      expect(getRank(c.castles8)).toBe(8);
      expect(getRank(c.stars9)).toBe(9);
      expect(getRank(c.gems10)).toBe(10);
    });
    test("picture cards have rank 11, 12, 13", () => {
      expect(getRank(c.swordsJ)).toBe(11);
      expect(getRank(c.castlesQ)).toBe(12);
      expect(getRank(c.starsK)).toBe(13);
    });
    test("ace has rank 14", () => {
      expect(getRank(c.gemsA)).toBe(14);
    });
  });

  test("getSuit", () => {
    expect(getSuit(c.gems2)).toBe("gems");
    expect(getSuit(c.swords3)).toBe("swords");
    expect(getSuit(c.castles4)).toBe("castles");
    expect(getSuit(c.stars5)).toBe("stars");
    expect(getSuit(c.gems6)).toBe("gems");
    expect(getSuit(c.swords7)).toBe("swords");
    expect(getSuit(c.castles8)).toBe("castles");
    expect(getSuit(c.stars9)).toBe("stars");
    expect(getSuit(c.gems10)).toBe("gems");
    expect(getSuit(c.swordsJ)).toBe("swords");
    expect(getSuit(c.gemsQ)).toBe("gems");
    expect(getSuit(c.swordsK)).toBe("swords");
    expect(getSuit(c.castlesA)).toBe("castles");
  });
});
