import { Player, PlayerId, GameState } from "../state";

export type Team = {
  index: number;
  members: [Player, Player];
  score: number;
};

export function findTeam(playerId: PlayerId, state: GameState): Team | null {
  const teams = getTeams(state);
  return teams.find((t) => t.members.some((p) => p.id === playerId)) ?? null;
}

export function getTeams(state: GameState): [Team, Team] {
  const team1: Team = {
    index: 0,
    members: [state.players[0], state.players[2]],
    score: state.score[0],
  };
  const team2: Team = {
    index: 1,
    members: [state.players[1], state.players[3]],
    score: state.score[1],
  };

  return [team1, team2];
}
