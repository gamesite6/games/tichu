import * as c from "./card";
import { uniq } from "lodash";

describe("cards", () => {
  test("there are 56 cards", () => {
    expect(c.cards.length).toBe(56);
  });
  test("all cards are different", () => {
    expect(uniq(c.cards).length).toBe(c.cards.length);
  });
});
