import { PlayerId } from "../state";
import { Card } from "./card";

export type Phase =
  | BigBetting
  | Pushing
  | Receiving
  | Leading
  | Following
  | RoundComplete
  | GameComplete;

export type BigBetting = {
  kind: "BigBetting";
  ready: PlayerId[];
  deck: Card[];
};

export type Pushing = {
  kind: "Pushing";
  pushed: { [fromPlayerId: number]: { [toPlayerId: number]: Card } };
};
export type Receiving = {
  kind: "Receiving";
  pushed: { [fromPlayerId: number]: { [toPlayerId: number]: Card } };
  ready: PlayerId[];
};

export type Leading = {
  kind: "Leading";
  player: PlayerId;

  /** player was given the lead by another player playing the dog */
  dog: boolean;

  /** players who have played all their cards, ordered first to last */
  finished: PlayerId[];
};

export type Following = {
  kind: "Following";

  /** Currently acting player */
  player: PlayerId;

  /** Player who played the latest (and highest) cards */
  leader: PlayerId;

  cards: Card[][];

  value: number;

  /** players who have played all their cards, ordered first to last */
  finished: PlayerId[];
};

export type RoundComplete = {
  kind: "RoundComplete";
  ready: PlayerId[];

  /** players who have played all their cards, ordered first to last */
  finished: PlayerId[];
};

export type GameComplete = {
  kind: "GameComplete";
};
