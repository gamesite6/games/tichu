const suits = ["Gems", "Swords", "Castles", "Stars"];
const ranks = [
  [2, "2"],
  [3, "3"],
  [4, "4"],
  [5, "5"],
  [6, "6"],
  [7, "7"],
  [8, "8"],
  [9, "9"],
  [10, "10"],
  [11, "J"],
  [12, "Q"],
  [13, "K"],
  [14, "A"],
];

const specials = [
  [-1, "Dog"],
  [1, "Sparrow"],
  [54, "Phoenix"],
  [55, "Dragon"],
];

console.log("export type Suit =");
suits.forEach((suit) => {
  console.log(`  | ${suit}`);
});

console.log("");

suits.forEach((suit) => {
  console.log(`export type ${suit} = "${suit.toLowerCase()}";`);
});

console.log("");

console.log("export type Card = SpecialCard | SuitedCard;");

console.log("");

console.log("export type SpecialCard =");
specials.forEach(([cardValue, name]) => {
  console.log(`  | ${name}`);
});

console.log("");

console.log("export type SuitedCard =");
suits.forEach((suit, suitIndex) => {
  ranks.forEach(([rankValue, rank]) => {
    const cardValue = rankValue + 13 * suitIndex;
    console.log(`  | ${suit}${rank}`);
  });
});

console.log("");

specials.forEach(([cardValue, name]) => {
  console.log(`export type ${name} = ${cardValue};`);
});

console.log("");

specials.forEach(([cardValue, name]) => {
  console.log(`export const ${name.toLowerCase()}: ${name} = ${cardValue};`);
});

console.log("");

ranks.forEach(([rankValue, rank], rankIndex) => {
  suits.forEach((suit, suitIndex) => {
    const cardValue = rankIndex * 4 + 2 + suitIndex;
    console.log(`export type ${suit}${rank} = ${cardValue};`);
    console.log(
      `export const ${suit.toLowerCase()}${rank}: ${suit}${rank} = ${cardValue}`
    );
  });
});

console.log("");

console.log(
  `export const cards: Card[] = [
  ${specials[0][1].toLowerCase()},
  ${specials[1][1].toLowerCase()},`
);
suits.forEach((suit, suitIndex) => {
  for (const [rankValue, rank] of ranks) {
    console.log(`  ${suit.toLowerCase()}${rank},`);
  }
});
console.log(`  ${specials[2][1].toLowerCase()},
  ${specials[3][1].toLowerCase()},
];
`);

console.log("Object.freeze(cards);");
