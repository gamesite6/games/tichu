import {
  SuitedCard,
  Card,
  SpecialCard,
  Suit,
  Sparrow,
  dragon,
  phoenix,
} from "./card";

export function isSpecial(card: Card): card is SpecialCard {
  return card < 2 || card > 53;
}
export function isSuited(card: Card): card is SuitedCard {
  return !isSpecial(card);
}

export function getSuit(card: SuitedCard): Suit {
  switch ((card - 2) % 4) {
    case 0:
      return "gems";
    case 1:
      return "swords";
    case 2:
      return "castles";
    case 3:
    default:
      return "stars";
  }
}

export function getRank(card: SuitedCard | Sparrow): number {
  return Math.floor((card - 2) / 4) + 2;
}

export function getScoreValue(card: Card): number {
  if (isSuited(card)) {
    const rank = getRank(card);
    if (rank === 5) {
      return 5;
    } else if (rank === 10) {
      return 10;
    } else if (rank === 13) {
      return 10;
    } else {
      return 0;
    }
  } else if (card === dragon) {
    return 25;
  } else if (card === phoenix) {
    return -25;
  } else {
    return 0;
  }
}
