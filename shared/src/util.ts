export function groupAdjacent<T>(
  arr: T[],
  isEqual: (a: T, b: T) => boolean = (a, b) => a === b
): T[][] {
  if (arr.length > 0) {
    let previousValue = arr[0];
    let currentArray = [previousValue];
    const result: T[][] = [currentArray];

    for (let i = 1; i < arr.length; i++) {
      const currentValue = arr[i];
      if (isEqual(previousValue, currentValue)) {
        previousValue = currentValue;
        currentArray.push(currentValue);
      } else {
        previousValue = currentValue;
        currentArray = [previousValue];
        result.push(currentArray);
      }
    }
    return result;
  } else {
    return [];
  }
}

export function sortAscending<T extends number>(arr: T[]): void {
  arr.sort((a, b) => a - b);
}

export function sortDescending<T extends number>(arr: T[]): void {
  arr.sort((a, b) => b - a);
}

export function sortedAscending<T extends number>(arr: T[]): T[] {
  const result = [...arr];
  sortAscending(result);
  return result;
}

export function sortedDescending<T extends number>(arr: T[]): T[] {
  const result = [...arr];
  sortDescending(result);
  return result;
}

export function combinations<T>(set: T[], k = set.length): T[][] {
  if (k > set.length) {
    return [];
  } else if (k === 1) {
    return set.map((t) => [t]);
  } else {
    const [head, ...tail] = set;

    return [
      ...combinations(tail, k - 1).map((c) => [head, ...c]),
      ...combinations(tail, k),
    ];
  }
}

export function windowSlices<T>(arr: T[], windowSize: number): T[][] {
  const windows: T[][] = [];

  for (let i = 0; i + windowSize <= arr.length; i++) {
    windows.push(arr.slice(i, i + windowSize));
  }

  return windows;
}

export function flatMapAll<T>(groups: T[][]): T[][] {
  const initial: T[][] = [];

  return groups.reduce((groups, current) => {
    if (groups.length === 0) {
      return current.map((e) => [e]);
    } else {
      return groups.flatMap((prevGroup) =>
        current.map((t) => [...prevGroup, t])
      );
    }
  }, initial);
}

export function hasDuplicates<T>(arr: T[]): boolean {
  const seen: Set<T> = new Set();
  for (let t of arr) {
    if (seen.has(t)) {
      return true;
    } else {
      seen.add(t);
    }
  }

  return false;
}
