import type { Action } from "./action";

export * from "./action";
export * from "./settings";
export * from "./state";

export type ActionHandler = (action: Action) => void;
