import { every, flatMap, groupBy, size, some, sortBy } from "lodash";
import { dragon, phoenix, sparrow } from "./state/card";
import type { Card, SuitedCard } from "./state/card";
import { getRank, getSuit, isSuited } from "./state/cardUtil";
import type { Phase, Leading, Following } from "./state/phase";
import {
  combinations,
  flatMapAll,
  groupAdjacent,
  hasDuplicates,
  sortAscending,
  sortDescending,
  sortedAscending,
  windowSlices,
} from "./util";

export * from "./state/card";
export * from "./state/cardUtil";
export * from "./state/phase";
export * from "./state/team";

export type PlayerId = number;

export type Player = {
  id: PlayerId;
  hand: Card[];
  bet?: Bet;
  claimed: Card[];
};
export type Bet = 100 | 200;

export type GameState = {
  players: Player[];
  score: [number, number];
  phase: Phase;
  wish?: number;
};

export type CardCombo = StandardCombo | Bomb;

export type StandardCombo =
  | "single"
  | "pair"
  | "pair-seq"
  | "trio"
  | "full-house"
  | "seq";

export type Bomb = "bomb";

function isPair(cards: Card[]): boolean {
  return (
    cards.length === 2 &&
    ((isSuited(cards[0]) &&
      isSuited(cards[1]) &&
      getRank(cards[0]) === getRank(cards[1])) ||
      cards[0] === phoenix ||
      cards[1] === phoenix)
  );
}

function isPairSequence(cards: Card[]): boolean {
  const validCards = cards.filter((card) => isSuited(card) || card === phoenix);

  if (validCards.length !== cards.length) {
    return false;
  }
  cards = validCards;

  const ranks: number[] = flatMap(cards, (card) => {
    if (isSuited(card)) {
      return [getRank(card)];
    } else {
      return [];
    }
  });

  ranks.sort((a, b) => a - b);

  const pairs = groupAdjacent(ranks);
  const connectedPairs = groupAdjacent(
    pairs,
    (a, b) => Math.abs(a[0] - b[0]) === 1
  );

  return (
    connectedPairs.length === 1 &&
    pairs.length === cards.length / 2 &&
    pairs.every((pair) => pair.length < 3)
  );
}

function isTrio(cards: Card[]): boolean {
  const suitedCard = cards.find(isSuited);
  if (cards.length === 3 && suitedCard) {
    return cards.every(
      (card) =>
        (isSuited(card) && getRank(card) === getRank(suitedCard)) ||
        card === phoenix
    );
  } else {
    return false;
  }
}

function isFullHouse(cards: Card[]): boolean {
  const groupedByRank = groupBy(cards, (card) =>
    isSuited(card) ? getRank(card) : card
  );
  return (
    cards.length === 5 &&
    ((size(groupedByRank) === 2 &&
      every(groupedByRank, (group) => group.length > 1)) ||
      (size(groupedByRank) === 3 &&
        some(groupedByRank, (group) => group[0] === phoenix)))
  );
}

function isSequence(cards: Card[]): boolean {
  if (cards.length < 5) {
    return false;
  }

  const validCards = cards.filter(
    (card) => isSuited(card) || card === sparrow || card === phoenix
  );

  if (validCards.length !== cards.length) {
    return false;
  }

  cards = validCards;

  let ranks: number[] = flatMap(cards, (card) => {
    if (isSuited(card)) {
      return [getRank(card)];
    } else if (card === sparrow) {
      return [1];
    } else {
      return [];
    }
  });
  sortAscending(ranks);

  const groups = groupAdjacent(ranks, (a, b) => Math.abs(a - b) === 1);

  if (groups.length === 1) {
    return true;
  } else if (groups.length === 2) {
    const hasPhoenix = cards.some((card) => card === phoenix);
    const firstGroup = groups[0];
    const lastGroup = groups[1];

    return hasPhoenix && lastGroup[0] - firstGroup[firstGroup.length - 1] === 2;
  } else {
    return false;
  }
}

export function isBomb(cards: Card[]): boolean {
  return isSuitedSequence(cards) || isQuad(cards);
}

function isSuitedSequence(cards: Card[]): boolean {
  const suitedCard = cards.find(isSuited);
  if (suitedCard) {
    const suit = getSuit(suitedCard);
    return (
      cards.every((card) => isSuited(card) && getSuit(card) === suit) &&
      isSequence(cards)
    );
  } else {
    return false;
  }
}

function isQuad(cards: Card[]): boolean {
  const firstCard = cards[0];

  return (
    cards.length === 4 &&
    isSuited(firstCard) &&
    cards.every(
      (card) => isSuited(card) && getRank(card) === getRank(firstCard)
    )
  );
}

export function classifyCombo(cards: Card[]): CardCombo | null {
  const n = cards.length;

  if (n < 1) return null;
  else if (n === 1) return "single";
  else if (isPair(cards)) return "pair";
  else if (isTrio(cards)) return "trio";
  else if (isSuitedSequence(cards) /* must be checked before normal sequence */)
    return "bomb";
  else if (isSequence(cards)) return "seq";
  else if (isPairSequence(cards)) return "pair-seq";
  else if (isFullHouse(cards)) return "full-house";
  else if (isQuad(cards)) return "bomb";
  else return null;
}

export function lastComboValue(
  combo: CardCombo,
  cards: Card[][]
): number | null {
  if (cards.length === 0) {
    return null;
  } else if (cards.length === 1) {
    return comboValue(combo, cards[0]);
  } else {
    const last = cards[cards.length - 1];

    if (combo === "single" && last[0] === phoenix) {
      const secondLast = cards[cards.length - 2];
      const previousValue = comboValue(combo, secondLast);
      return previousValue && comboValue(combo, last, previousValue);
    } else {
      return comboValue(combo, last);
    }
  }
}

export function comboValue(
  combo: CardCombo,
  cards: Card[],
  previousValue: number = 1
): number | null {
  if (combo === "single") {
    const card = cards[0];
    if (isSuited(card)) {
      return getRank(card);
    } else if (card === phoenix) {
      return Math.min(14.5, previousValue + 0.5);
    } else if (card === dragon) {
      return 15;
    } else if (card === sparrow) {
      return 1;
    } else {
      return null;
    }
  } else {
    const suitCardRanks: number[] = flatMap(cards, (card) =>
      isSuited(card) ? [getRank(card)] : []
    );
    sortDescending(suitCardRanks);

    switch (combo) {
      case "pair":
        return suitCardRanks[0];
      case "trio":
        return suitCardRanks[0];
      case "bomb":
        if (cards.length < 5) {
          return 100 * suitCardRanks[0];
        } else {
          return 1000 * cards.length * suitCardRanks[0];
        }
      case "full-house":
        const grouped = groupAdjacent(suitCardRanks);
        return (
          grouped.find((group) => group.length === 3)?.[0] ?? suitCardRanks[0]
        );

      case "seq":
        const hasPhoenix = cards.some((card) => card === phoenix);
        const runs = groupAdjacent(
          suitCardRanks,
          (a, b) => Math.abs(a - b) === 1
        );
        if (runs.length === 1 && hasPhoenix) {
          return Math.min(suitCardRanks[0] + 1, 14);
        } else {
          return suitCardRanks[0];
        }

      case "pair-seq":
        return suitCardRanks[0];
    }
  }
}

export function getPossiblePlays(
  previous: Card[],
  prevValue: number,
  hand: Card[]
): Array<Card[]> {
  const combo = classifyCombo(previous);

  if (!combo) return [];

  const size = previous.length;
  const bombs = findBombs(hand);
  const isBetter = betterThan(combo, prevValue);

  switch (combo) {
    case "single":
    case "pair":
    case "pair-seq":
    case "trio":
    case "full-house":
    case "seq": {
      const plays = findStandardCombos(hand, combo, size).filter(isBetter);
      return [...plays, ...bombs];
    }

    case "bomb":
      return bombs.filter(isBetter);
  }
}

const betterThan =
  (previousCombo: CardCombo, previousValue: number) =>
  (cards: Card[]): boolean => {
    const value = comboValue(previousCombo, cards, previousValue);
    return value !== null && value > previousValue;
  };

function phoenixCount(cards: Card[]): number {
  let count = 0;
  for (let card of cards) {
    if (card === phoenix) count++;
  }
  return count;
}

function addPhoenixToRankGroups(groups: SuitedCard[][]): Card[][] {
  if (groups.length === 0) return [];

  const ranks = new Map<number, Card[]>();

  for (let grp of groups) {
    const rank = getRank(grp[0]);
    ranks.set(rank, grp);
  }

  const ranksWithPhoenix = new Map<number, Card[]>();

  for (let rank = 2; rank <= 13; rank++) {
    if (ranks.has(rank - 1) || ranks.has(rank) || ranks.has(rank + 1)) {
      const grp = ranks.get(rank) ?? [];
      ranksWithPhoenix.set(rank, [...grp, phoenix]);
    }
  }

  const arr = Array.from(ranksWithPhoenix.entries());

  return sortBy(arr, ([rank, _grp]) => rank).map(([_rank, grp]) => grp);
}

export function findStandardCombos(
  hand: Card[],
  combo: StandardCombo,
  length?: number
): Card[][] {
  if (combo === "single") {
    return hand.map((card) => [card]);
  }

  const sortedHand = sortedAscending(hand);

  const suitedCards = sortedHand.filter((c) => isSuited(c)) as SuitedCard[];

  const rankGroups = groupAdjacent(
    suitedCards,
    (a, b) => getRank(a) === getRank(b)
  );

  const hasPhoenix = hand.some((c) => c === phoenix);

  switch (combo) {
    case "pair": {
      const pairs: Card[][] = rankGroups.flatMap((cards) =>
        combinations(cards, 2)
      );

      if (hasPhoenix) {
        for (const card of suitedCards) {
          pairs.push([card, phoenix]);
        }
      }

      return pairs;
    }
    case "pair-seq": {
      if (!length) return [];

      const pairGroups = (
        hasPhoenix ? addPhoenixToRankGroups(rankGroups) : rankGroups
      )
        .filter((grp) => grp.length >= 2)
        .map((grp) => combinations(grp, 2));

      const pairRuns = groupAdjacent(
        pairGroups,
        (groupA, groupB) =>
          getRank(groupA[0][0] as SuitedCard) + 1 ===
          getRank(groupB[0][0] as SuitedCard)
      );

      const correctLengthRuns = pairRuns.map((run) =>
        windowSlices(run, length / 2)
      );

      const plays = correctLengthRuns.flatMap((runs) =>
        runs.flatMap((run) => flatMapAll(run).map((pairs) => pairs.flat()))
      );

      return plays.filter((cards) => !hasDuplicates(cards));
    }
    case "trio": {
      const trios: Card[][] = rankGroups.flatMap((cards) =>
        combinations(cards, 3)
      );

      if (hasPhoenix) {
        const pairs: Card[][] = rankGroups.flatMap((cards) =>
          combinations(cards, 2)
        );

        for (const cards of pairs) {
          cards.push(phoenix);
          trios.push(cards);
        }
      }

      return trios;
    }
    case "full-house": {
      const groups = hasPhoenix
        ? addPhoenixToRankGroups(rankGroups as SuitedCard[][])
        : rankGroups;

      const twos = groups
        .filter((grp) => grp.length >= 2)
        .flatMap((grp) => combinations(grp, 2));

      const threes = groups
        .filter((grp) => grp.length >= 3)
        .flatMap((grp) => combinations(grp, 3));

      return flatMapAll([twos, threes])
        .map((grps) => grps.flat())
        .filter((cards) => !hasDuplicates(cards));
    }
    case "seq": {
      if (!length) return [];

      const runs = groupAdjacent(
        hasPhoenix ? addPhoenixToRankGroups(rankGroups) : rankGroups,
        (groupA, groupB) =>
          groupA[0] === phoenix ||
          groupB[0] === phoenix ||
          getRank(groupA[0] as SuitedCard) + 1 ===
            getRank(groupB[0] as SuitedCard)
      );

      const runSlices = runs.flatMap((run) => windowSlices(run, length));

      const runCombinations = runSlices.flatMap((grps) => flatMapAll(grps));

      return runCombinations.filter((cards) => phoenixCount(cards) < 2);
    }
  }
}

export function findBombs(hand: Card[]): Card[][] {
  const suitedCards = sortedAscending(hand).filter((c) =>
    isSuited(c)
  ) as SuitedCard[];

  let rankGroups = groupBy(suitedCards, getRank);

  const quads = Object.values(rankGroups).filter((grp) => grp.length === 4);

  const suitGroups = groupBy(suitedCards, getSuit);

  const sortedSuits = Object.values(suitGroups).map((grp) =>
    sortedAscending(grp)
  );

  const suitedRuns = sortedSuits.map((suitGrp) =>
    groupAdjacent(
      suitGrp,
      (cardA, cardB) => getRank(cardA) + 1 === getRank(cardB)
    )
  );

  const straightFlushes = suitedRuns.flatMap((suitGrp) =>
    suitGrp.filter((runs) => runs.length >= 5)
  );

  return [...quads, ...straightFlushes];
}

export function mustFulfillWish(
  phase: Leading | Following,
  user: Player,
  wish: number
): boolean {
  if (user.id !== phase.player) return false;

  switch (phase.kind) {
    case "Following": {
      const previous = phase.cards[phase.cards.length - 1];
      const combo = classifyCombo(previous);
      const previousValue = combo && lastComboValue(combo, phase.cards);

      if (!previousValue) return false;

      const plays = getPossiblePlays(previous, previousValue, user.hand);

      return plays.some((cards) =>
        cards.some((card) => isSuited(card) && getRank(card) === wish)
      );
    }
    case "Leading": {
      return user.hand.some((card) => isSuited(card) && getRank(card) === wish);
    }
  }
}

export function fulfillsWish(wish: number, cards: Card[]): boolean {
  return cards.some((card) => isSuited(card) && getRank(card) === wish);
}
