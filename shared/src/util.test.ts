import { combinations, flatMapAll, groupAdjacent, windowSlices } from "./util";
import { groupBy } from "lodash";

describe("groupAdjacent", () => {
  test("groups numbers", () => {
    expect(groupAdjacent([1, 1, 2, 2, 3, 3])).toEqual([
      [1, 1],
      [2, 2],
      [3, 3],
    ]);
  });
  test("groups sequence into single element groups", () => {
    expect(groupAdjacent([1, 2, 3, 4, 5])).toEqual([[1], [2], [3], [4], [5]]);
  });
  test("no groups for empty array", () => {
    expect(groupAdjacent([])).toEqual([]);
  });
});

describe("combinations", () => {
  test("3-combination of [1,2,3,4,5]", () => {
    expect(combinations([1, 2, 3, 4, 5], 3)).toEqual([
      [1, 2, 3],
      [1, 2, 4],
      [1, 2, 5],
      [1, 3, 4],
      [1, 3, 5],
      [1, 4, 5],
      [2, 3, 4],
      [2, 3, 5],
      [2, 4, 5],
      [3, 4, 5],
    ]);
  });
});

describe("groupBy", () => {
  test("group values by key", () => {
    expect(
      groupBy([3, 6, 1, 2, 9, 7, 4], (x) => (x % 2 === 0 ? "even" : "odd"))
    ).toEqual({
      even: [6, 2, 4],
      odd: [3, 1, 9, 7],
    });
  });
});

describe("flatMapAll", () => {
  test("gets all combinations of one element from each row", () => {
    expect(flatMapAll([[1], [2, 3], [4, 5, 6]])).toEqual([
      [1, 2, 4],
      [1, 2, 5],
      [1, 2, 6],
      [1, 3, 4],
      [1, 3, 5],
      [1, 3, 6],
    ]);

    expect(flatMapAll([[1, 2], [3], [4, 5]])).toEqual([
      [1, 3, 4],
      [1, 3, 5],
      [2, 3, 4],
      [2, 3, 5],
    ]);
  });
});

describe("windowSlices", () => {
  test("gets all slices of length n", () => {
    expect(windowSlices([1, 2, 3, 4, 5], 3)).toEqual([
      [1, 2, 3],
      [2, 3, 4],
      [3, 4, 5],
    ]);
  });
});
