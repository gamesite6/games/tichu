export type GameSettings = {
  gameLength: GameLength;
  tiebreaker: Tiebreaker;
};
export type GameLength = "standard-game" | "shorter-game" | "single-round";
export type Tiebreaker = "play-another-round" | "no-tiebreaker";
