import {
  getPossiblePlays,
  classifyCombo,
  comboValue,
  findBombs,
} from "./state";
import * as c from "./state/card";

describe("card combinations", () => {
  test("single dragon is highest value", () => {
    const cards = [c.dragon];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("single");
    expect(combo && comboValue(combo, cards, 6)).toBe(15);
  });

  test("single phoenix is half a point over previous", () => {
    const cards = [c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("single");
    expect(combo && comboValue(combo, cards, 6)).toBe(6.5);
  });

  test("phoenix can beat single ace", () => {
    const cards = [c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("single");
    expect(combo && comboValue(combo, cards, 14)).toBe(14.5);
  });

  test("phoenix can not beat dragon", () => {
    const cards = [c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("single");
    expect(combo && comboValue(combo, cards, 15)).toBe(14.5);
  });

  test("leading with phoenix has value 1.5", () => {
    expect(comboValue("single", [c.phoenix])).toBe(1.5);
  });

  test("pair", () => {
    const cards = [c.stars2, c.gems2];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("pair");
    expect(combo && comboValue(combo, cards)).toBe(2);
  });

  test("pair with phoenix", () => {
    const cards = [c.stars6, c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("pair");
    expect(combo && comboValue(combo, cards)).toBe(6);
  });

  test("no pair", () => {
    expect(classifyCombo([c.stars7, c.gems5])).toBeNull();
  });

  test("trio", () => {
    const cards = [c.stars5, c.gems5, c.castles5];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("trio");
    expect(combo && comboValue(combo, cards)).toBe(5);
  });

  test("trio with phoenix", () => {
    const cards = [c.stars5, c.phoenix, c.castles5];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("trio");
    expect(combo && comboValue(combo, cards)).toBe(5);
  });

  test("trio starting with phoenix", () => {
    const cards = [c.phoenix, c.starsA, c.gemsA];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("trio");
    expect(combo && comboValue(combo, cards)).toBe(14);
  });

  test("no trio", () => {
    expect(classifyCombo([c.stars5, c.gems6, c.castles5])).toBeNull();
  });

  test("no trio with phoenix", () => {
    expect(classifyCombo([c.stars5, c.phoenix, c.castles6])).toBeNull();
  });

  test("full house", () => {
    const cards = [c.stars7, c.gems7, c.castles7, c.gems5, c.swords5];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("full-house");
    expect(combo && comboValue(combo, cards)).toBe(7);
  });

  test("full house with phoenix", () => {
    const cards = [c.stars7, c.gems7, c.phoenix, c.gems5, c.swords5];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("full-house");
    expect(combo && comboValue(combo, cards)).toBe(7);
  });

  test("sequence", () => {
    const cards = [c.stars2, c.gems3, c.castles4, c.gems5, c.swords6];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toBe(6);
  });

  test("not sequence", () => {
    expect(
      classifyCombo([c.stars2, c.gems3, c.castles4, c.gems5, c.swords7])
    ).toBeNull();
  });

  test("sequence with mahjong", () => {
    const cards = [c.sparrow, c.gems2, c.castles3, c.gems4, c.swords5];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toBe(5);
  });

  test("unsorted long sequence", () => {
    const cards = [
      c.gems6,
      c.swordsA,
      c.swords9,
      c.castles7,
      c.gems10,
      c.gemsJ,
      c.starsK,
      c.gems8,
      c.castlesQ,
    ];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toBe(14);
  });

  test("phoenix may not extend sequence beyond ace", () => {
    const cards = [c.gemsJ, c.swordsQ, c.swordsK, c.castlesA, c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toBe(14);
  });

  test("unsorted long sequence with phoenix", () => {
    const cards = [
      c.gems6,
      c.swordsA,
      c.swords9,
      c.phoenix,
      c.gems10,
      c.gemsJ,
      c.starsK,
      c.gems8,
      c.castlesQ,
    ];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toBe(14);
  });

  test("sequence with phoenix at end", () => {
    const cards = [c.gems6, c.gems8, c.swords9, c.castles7, c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("seq");
    expect(combo && comboValue(combo, cards)).toEqual(10);
  });

  test("sequence must be at least 5 cards", () => {
    expect(
      classifyCombo([c.gems8, c.swords9, c.castles7, c.phoenix])
    ).toBeNull();
  });

  test("pair sequence", () => {
    const cards = [
      c.gems2,
      c.stars2,
      c.swords3,
      c.castles3,
      c.gems4,
      c.swords4,
    ];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("pair-seq");
    expect(combo && comboValue(combo, cards)).toBe(4);
  });

  test("pair sequence with phoenix", () => {
    const cards = [
      c.gems2,
      c.stars2,
      c.swords3,
      c.castles3,
      c.phoenix,
      c.swords4,
    ];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("pair-seq");
    expect(combo && comboValue(combo, cards)).toBe(4);
  });

  test("pair sequence may not have a gap", () => {
    expect(
      classifyCombo([
        c.gems2,
        c.swords2,
        c.swords3,
        c.castles3,
        c.gems5,
        c.swords5,
      ])
    ).toBeNull();
  });

  test("not a pair sequence", () => {
    const cards = [c.gemsK, c.gemsA, c.swordsA, c.castlesA];
    expect(classifyCombo(cards)).toBeNull();
  });

  test("suited sequence bomb", () => {
    const cards = [c.stars3, c.stars4, c.stars5, c.stars6, c.stars7];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("bomb");
    expect(combo && comboValue(combo, cards)).toBe(35000);
  });

  test("suited sequence bomb may not use phoenix", () => {
    const cards = [c.stars3, c.stars4, c.phoenix, c.stars6, c.stars7];
    const combo = classifyCombo(cards);
    expect(combo).not.toEqual("bomb");
  });

  test("there is no five of a kind", () => {
    expect(
      classifyCombo([c.swords7, c.stars7, c.castles7, c.gems7, c.phoenix])
    ).toBeNull();
  });

  test("quad", () => {
    const cards = [c.swords7, c.stars7, c.castles7, c.gems7];
    const combo = classifyCombo(cards);
    expect(combo).toEqual("bomb");
    expect(combo && comboValue(combo, cards)).toBe(700);
  });

  test("quads may not include phoenix", () => {
    const cards = [c.swords7, c.stars7, c.castles7, c.phoenix];
    const combo = classifyCombo(cards);
    expect(combo).toBeNull();
  });
});

describe("possible plays", () => {
  test("single", () => {
    const previous = [c.stars9];
    const previousValue = 9;
    const hand = [c.dog, c.gems5, c.castlesJ, c.swordsA, c.phoenix, c.dragon];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.castlesJ],
      [c.swordsA],
      [c.phoenix],
      [c.dragon],
    ]);
  });

  test("single, with bomb", () => {
    const previous = [c.stars9];
    const previousValue = 9;
    const hand = [
      c.dog,
      c.gems5,
      c.gemsJ,
      c.swordsJ,
      c.castlesJ,
      c.starsJ,
      c.swordsA,
      c.phoenix,
      c.dragon,
    ];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gemsJ],
      [c.swordsJ],
      [c.castlesJ],
      [c.starsJ],
      [c.swordsA],
      [c.phoenix],
      [c.dragon],
      [c.gemsJ, c.swordsJ, c.castlesJ, c.starsJ],
    ]);
  });

  test("pair", () => {
    const previous = [c.swords3, c.gems3];
    const previousValue = 3;

    const hand = [c.castles2, c.castles4, c.gems4, c.stars8, c.phoenix];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gems4, c.castles4],
      [c.gems4, c.phoenix],
      [c.castles4, c.phoenix],
      [c.stars8, c.phoenix],
    ]);
  });

  test("trio", () => {
    const previous = [c.gems4, c.swords4, c.castles4];
    const previousValue = 4;

    const hand = [c.gems6, c.swords6, c.starsK, c.gemsK, c.swordsK, c.phoenix];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gemsK, c.swordsK, c.starsK],
      [c.gems6, c.swords6, c.phoenix],
      [c.gemsK, c.swordsK, c.phoenix],
      [c.gemsK, c.starsK, c.phoenix],
      [c.swordsK, c.starsK, c.phoenix],
    ]);
  });

  test("seq, no phoenix", () => {
    const previous = [c.gems4, c.swords5, c.castles6, c.stars7, c.castles8];
    const previousValue = comboValue("seq", previous) ?? -1;

    const hand = [
      c.stars4,
      c.gems5,
      c.stars6,
      c.gems7,
      c.swords8,
      c.stars9,
      c.gems10,
    ];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gems5, c.stars6, c.gems7, c.swords8, c.stars9],
      [c.stars6, c.gems7, c.swords8, c.stars9, c.gems10],
    ]);
  });

  test("seq, gap, with phoenix", () => {
    const previous = [c.gems4, c.swords5, c.castles6, c.stars7, c.castles8];
    const previousValue = comboValue("seq", previous) ?? -1;

    const hand = [
      c.stars4,
      c.gems5,
      c.stars6,
      c.gems7,
      c.stars9,
      c.gems10,
      c.phoenix,
    ];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gems5, c.stars6, c.gems7, c.phoenix, c.stars9],
      [c.stars6, c.gems7, c.phoenix, c.stars9, c.gems10],
    ]);
  });

  test("seq, no gap, with phoenix", () => {
    const previous = [c.gems4, c.swords5, c.castles6, c.stars7, c.castles8];
    const previousValue = comboValue("seq", previous) ?? -1;

    const hand = [c.stars6, c.gems7, c.swords8, c.stars9, c.gems10, c.phoenix];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.phoenix, c.stars6, c.gems7, c.swords8, c.stars9],
      [c.stars6, c.gems7, c.swords8, c.stars9, c.gems10],
      [c.stars6, c.gems7, c.swords8, c.stars9, c.phoenix],
      [c.stars6, c.gems7, c.swords8, c.phoenix, c.gems10],
      [c.stars6, c.gems7, c.phoenix, c.stars9, c.gems10],
      [c.stars6, c.phoenix, c.swords8, c.stars9, c.gems10],
      [c.phoenix, c.gems7, c.swords8, c.stars9, c.gems10],
      [c.gems7, c.swords8, c.stars9, c.gems10, c.phoenix],
    ]);
  });

  test("full house, no phoenix", () => {
    const previous = [c.gems2, c.swords2, c.swords3, c.castles3, c.stars3];
    const previousValue = comboValue("full-house", previous) ?? -1;

    const hand = [c.gems4, c.swords4, c.castles4, c.gems5, c.swords5, c.stars5];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gems4, c.swords4, c.gems5, c.swords5, c.stars5],
      [c.gems4, c.castles4, c.gems5, c.swords5, c.stars5],
      [c.swords4, c.castles4, c.gems5, c.swords5, c.stars5],
      [c.gems5, c.swords5, c.gems4, c.swords4, c.castles4],
      [c.gems5, c.stars5, c.gems4, c.swords4, c.castles4],
      [c.swords5, c.stars5, c.gems4, c.swords4, c.castles4],
    ]);
  });

  test("full house, with phoenix", () => {
    const previous = [c.gems2, c.swords2, c.swords3, c.castles3, c.stars3];
    const previousValue = comboValue("full-house", previous) ?? -1;

    const hand = [c.gems4, c.swords4, c.gems5, c.swords5, c.phoenix];
    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.gems4, c.swords4, c.gems5, c.swords5, c.phoenix],
      [c.gems5, c.swords5, c.gems4, c.swords4, c.phoenix],
    ]);
  });

  test("pair seq, no phoenix", () => {
    const previous = [c.swords3, c.castles3, c.gems4, c.swords4];
    const previousValue = comboValue("pair-seq", previous) ?? -1;

    const hand = [
      c.gems4,
      c.swords4,
      c.castles4,
      c.gems5,
      c.swords5,
      c.gems8,
      c.swords8,
      c.gems9,
      c.swords9,
      c.castles10,
      c.stars10,
    ];
    const possible = getPossiblePlays(previous, previousValue, hand);
    expect(possible).toEqual([
      [c.gems4, c.swords4, c.gems5, c.swords5],
      [c.gems4, c.castles4, c.gems5, c.swords5],
      [c.swords4, c.castles4, c.gems5, c.swords5],
      [c.gems8, c.swords8, c.gems9, c.swords9],
      [c.gems9, c.swords9, c.castles10, c.stars10],
    ]);
  });

  test("pair seq, with phoenix", () => {
    const previous = [c.swords3, c.castles3, c.gems4, c.swords4];
    const previousValue = comboValue("pair-seq", previous) ?? -1;

    const hand = [
      c.gems4,
      c.gems5,
      c.swords5,
      c.gems8,
      c.swords8,
      c.gems9,
      c.swords9,
      c.phoenix,
    ];
    const possible = getPossiblePlays(previous, previousValue, hand);
    expect(possible).toEqual([
      [c.gems4, c.phoenix, c.gems5, c.swords5],
      [c.gems8, c.swords8, c.gems9, c.swords9],
      [c.gems8, c.swords8, c.gems9, c.phoenix],
      [c.gems8, c.swords8, c.swords9, c.phoenix],
      [c.gems8, c.phoenix, c.gems9, c.swords9],
      [c.swords8, c.phoenix, c.gems9, c.swords9],
    ]);
  });

  test("bomb", () => {
    const previous = [c.gems3, c.gems4, c.gems5, c.gems6, c.gems7];
    const previousValue = comboValue("bomb", previous) ?? -1;

    const hand = [
      c.stars3,
      c.stars4,
      c.stars5,
      c.stars6,
      c.stars7,
      c.castles2,
      c.castles3,
      c.castles4,
      c.castles5,
      c.castles6,
      c.castles7,
      c.gems7,
      c.stars7,
      c.swords7,
    ];

    const possible = getPossiblePlays(previous, previousValue, hand);

    expect(possible).toEqual([
      [c.castles2, c.castles3, c.castles4, c.castles5, c.castles6, c.castles7],
    ]);
  });
});

describe("findBombs", () => {
  test("finds quads and (maximal) straight flushes", () => {
    const bombs = findBombs([
      c.gems7,
      c.stars2,
      c.gems8,
      c.castles2,
      c.swords2,
      c.gems2,
      c.gems3,
      c.gems6,
      c.gems4,
      c.gems5,
    ]);

    expect(bombs).toEqual([
      [c.gems2, c.swords2, c.castles2, c.stars2],
      [c.gems2, c.gems3, c.gems4, c.gems5, c.gems6, c.gems7, c.gems8],
    ]);
  });
});
