import { Card, PlayerId } from "./state";

export type Action =
  | { kind: "Pass" }
  | { kind: "BetBig" }
  | { kind: "BetSmall" }
  | { kind: "Push"; pushed: { [playerId: number]: Card } }
  | { kind: "Ready" }
  | { kind: "Play"; cards: Card[]; wish?: number }
  | { kind: "Bomb"; cards: Card[] }
  | { kind: "Claim"; giveTo?: PlayerId };
