import type { prng } from "seedrandom";

export function shuffle(rng: prng, array: any[]): void {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(rng() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}
