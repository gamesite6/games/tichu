import {
  Action,
  Bomb,
  Card,
  classifyCombo,
  comboValue,
  dragon,
  findTeam,
  getScoreValue,
  getTeams,
  BigBetting,
  dog,
  sparrow,
  Player,
  PlayerId,
  Receiving,
  GameSettings,
  GameState,
  fulfillsWish,
  mustFulfillWish,
} from "./shared";
import { sum } from "lodash";
import * as seedrandom from "seedrandom";
import { getSortedDeck } from "./initialState";
import { shuffle } from "./util";

function assertNever(x: never): never {
  throw new Error("Unexpected object: " + x);
}

function todo(description?: string): any {
  throw new Error(
    description ? `unimplemented: ${description}` : "unimplemented"
  );
}

export function isCompleted(state: GameState): boolean {
  return state.phase.kind === "GameComplete";
}

function actionIsAllowed({
  action,
  performedBy,
  state,
}: {
  action: Action;
  performedBy: PlayerId;
  settings: GameSettings;
  state: GameState;
}): boolean {
  const user = state.players.find((p) => p.id === performedBy);
  if (!user) {
    return false;
  }
  const phase = state.phase;

  switch (phase.kind) {
    case "BigBetting":
      switch (action.kind) {
        case "Pass":
        case "BetBig":
          return !phase.ready.includes(performedBy);

        case "BetSmall":
        case "Push":
        case "Ready":
        case "Play":
        case "Bomb":
        case "Claim":
          return false;
        default:
          assertNever(action);
      }

    case "Pushing":
      switch (action.kind) {
        case "Pass":
        case "BetBig":
          return false;

        case "BetSmall":
          return !user.bet;

        case "Push":
          const pushedCards: Card[] = Object.values(action.pushed);
          const otherPlayers = state.players.filter(
            (player) => player.id !== user.id
          );

          return (
            !phase.pushed[user.id] &&
            pushedCards.length === 3 &&
            otherPlayers.every((player) => action.pushed[player.id]) &&
            pushedCards.every((card) => user.hand.includes(card))
          );

        case "Ready":
        case "Play":
        case "Bomb":
        case "Claim":
          return false;
        default:
          assertNever(action);
      }
    case "Receiving":
      switch (action.kind) {
        case "Pass":
        case "BetSmall":
        case "BetBig":
        case "Push":
          return false;

        case "Ready":
          return !phase.ready.includes(user.id);
        case "Play":
        case "Bomb":
        case "Claim":
          return false;
        default:
          assertNever(action);
      }
    case "Leading":
      switch (action.kind) {
        case "Pass":
        case "BetBig":
        case "Push":
        case "Ready":
          return false;
        case "BetSmall":
          return !user.bet && user.hand.length === 14;
        case "Play":
          const combo = classifyCombo(action.cards);

          const mustFulfill =
            state.wish !== undefined &&
            mustFulfillWish(phase, user, state.wish);

          const fulfills =
            state.wish === undefined || fulfillsWish(state.wish, action.cards);

          return (
            combo !== null &&
            user.id === phase.player &&
            action.cards.every((card) => user.hand.includes(card)) &&
            (!mustFulfill || fulfills)
          );
        case "Bomb": {
          const combo = classifyCombo(action.cards);
          return (
            combo === "bomb" &&
            user.id === phase.player &&
            action.cards.every((card) => user.hand.includes(card))
          );
        }
        case "Claim":
          return false;
        default:
          assertNever(action);
      }

    case "Following":
      switch (action.kind) {
        case "Pass":
          return (
            user.id === phase.player &&
            user.id !== phase.leader &&
            (!state.wish || !mustFulfillWish(phase, user, state.wish))
          );

        case "BetBig":
        case "Push":
        case "Ready":
          return false;

        case "BetSmall":
          return !user.bet && user.hand.length === 14;
        case "Play":
          const previousCards = phase.cards[phase.cards.length - 1];
          const previousCombo = classifyCombo(previousCards);
          const previousValue = phase.value;

          const combo = classifyCombo(action.cards);

          if (combo === null) {
            return false;
          }

          const value = comboValue(combo, action.cards, previousValue);
          if (value === null) {
            return false;
          }

          const mustFulfill =
            state.wish !== undefined &&
            mustFulfillWish(phase, user, state.wish);

          const fulfills =
            state.wish === undefined || fulfillsWish(state.wish, action.cards);

          return (
            user.id === phase.player &&
            action.cards.every((card) => user.hand.includes(card)) &&
            ((combo === previousCombo &&
              action.cards.length === previousCards.length) ||
              combo === "bomb") &&
            value > previousValue &&
            (!mustFulfill || fulfills)
          );
        case "Bomb": {
          const previousCombo = classifyCombo(
            phase.cards[phase.cards.length - 1]
          );
          const previousValue = phase.value;

          const combo = classifyCombo(action.cards);
          if (combo !== "bomb") {
            return false;
          }

          const value = comboValue(combo, action.cards, previousValue);
          if (value === null) {
            return false;
          }

          return (
            action.cards.every((card) => user.hand.includes(card)) &&
            (previousCombo !== "bomb" || value > previousValue)
          );
        }
        case "Claim": {
          const winningCombo = phase.cards[phase.cards.length - 1];

          if (
            classifyCombo(winningCombo) === "single" &&
            winningCombo[0] === dragon
          ) {
            const teams = getTeams(state);
            const opponentTeam = teams.find(
              (team) => !team.members.some((p) => p.id === user.id)
            );
            if (opponentTeam === undefined) {
              return false;
            }

            return (
              user.id === phase.player &&
              user.id === phase.leader &&
              opponentTeam.members.some((p) => p.id === action.giveTo)
            );
          } else {
            return user.id === phase.player && user.id === phase.leader;
          }
        }

        default:
          assertNever(action);
      }
    case "RoundComplete": {
      switch (action.kind) {
        case "Pass":
        case "BetBig":
        case "BetSmall":
        case "Push":
        case "Play":
        case "Bomb":
        case "Claim":
          return false;

        case "Ready": {
          return !phase.ready.includes(user.id);
        }
      }
    }
    case "GameComplete": {
      return false;
    }
    default:
      return assertNever(phase);
  }
}

export default function performAction({
  seed,
  settings,
  performedBy,
  action,
  state,
}: {
  seed: number;
  settings: GameSettings;
  performedBy: PlayerId;
  action: Action;
  state: GameState;
}): GameState | null {
  if (!actionIsAllowed({ action, performedBy, state, settings })) {
    return null;
  }

  switch (state.phase.kind) {
    case "BigBetting":
      switch (action.kind) {
        case "Pass":
          state.phase.ready.push(performedBy);
          if (state.phase.ready.length === state.players.length) {
            completeBigBettingPhase(state, state.phase);
          }
          return state;
        case "BetBig":
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            user.bet = 200;
          }

          state.phase.ready.push(performedBy);
          if (state.phase.ready.length === state.players.length) {
            completeBigBettingPhase(state, state.phase);
          }
          return state;

        case "BetSmall":
        case "Push":
        case "Ready":
        case "Play":
        case "Bomb":
        case "Claim":
          return null;
        default:
          assertNever(action);
      }

    case "Pushing":
      switch (action.kind) {
        case "Pass":
        case "BetBig":
          return null;

        case "BetSmall": {
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            user.bet = 100;

            const otherPlayers = state.players.filter((p) => p.id !== user.id);
            /* "unpushing" cards for the other players, so they may make adjustments */
            for (const player of otherPlayers) {
              delete state.phase.pushed[player.id];
            }
          }

          return state;
        }

        case "Push":
          state.phase.pushed[performedBy] = action.pushed;

          const user = state.players.find((p) => p.id === performedBy);

          if (Object.values(state.phase.pushed).length === 4) {
            for (const player of state.players) {
              removeCardsFromHand(
                player,
                Object.values(state.phase.pushed[player.id])
              );
            }
            const nextPhase: Receiving = {
              kind: "Receiving",
              pushed: state.phase.pushed,
              ready: [],
            };
            state.phase = nextPhase;
            return state;
          } else {
            return state;
          }

        case "Ready":
        case "Play":
        case "Bomb":
        case "Claim":
          return null;
        default:
          assertNever(action);
      }
    case "Receiving":
      switch (action.kind) {
        case "Pass":
        case "BetSmall":
        case "BetBig":
        case "Push":
          return null;

        case "Ready":
          state.phase.ready.push(performedBy);
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            for (const fromPlayerId in state.phase.pushed) {
              for (const toPlayerId in state.phase.pushed[fromPlayerId]) {
                if (parseInt(toPlayerId) === user.id) {
                  user.hand.push(state.phase.pushed[fromPlayerId][toPlayerId]);
                }
              }
            }
          }

          if (state.phase.ready.length === 4) {
            const mahjongPlayer = state.players.find((p) =>
              p.hand.some((c) => c === sparrow)
            );

            state.phase = {
              kind: "Leading",
              player: mahjongPlayer?.id ?? state.players[0].id,
              dog: false,
              finished: [],
            };
          }

          return state;
        case "Play":
        case "Bomb":
        case "Claim":
          return null;
        default:
          assertNever(action);
      }
    case "Leading": {
      const phase = state.phase;

      switch (action.kind) {
        case "Pass":
        case "BetBig":
        case "Push":
        case "Ready":
          return null;

        case "BetSmall": {
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            user.bet = 100;
          }
          return state;
        }

        case "Play": {
          const user = state.players.find((p) => p.id === performedBy);
          if (!user) return null;

          if (
            state.wish !== undefined &&
            fulfillsWish(state.wish, action.cards)
          ) {
            delete state.wish;
          }

          removeCardsFromHand(user, action.cards);
          if (handEmpty(user)) {
            phase.finished.push(user.id);
          }

          const doubleVictory =
            phase.finished.length === 2 &&
            findTeam(user.id, state)?.members.every((member) =>
              phase.finished.includes(member.id)
            );

          if (doubleVictory) {
            state.phase = {
              kind: "RoundComplete",
              finished: phase.finished,
              ready: [],
            };
            return state;
          }

          const combo = classifyCombo(action.cards);

          if (combo === "single" && action.cards[0] === dog) {
            state.phase = {
              kind: "Leading",
              player: getDogTarget(performedBy, state.players),
              dog: true,
              finished: state.phase.finished,
            };
            return state;
          }

          const value = combo && comboValue(combo, action.cards);
          if (!value) return null;

          state.wish =
            state.wish ??
            (action.cards.some((c) => c === sparrow) ? action.wish : undefined);

          state.phase = {
            kind: "Following",
            player: getNextPlayer(performedBy, state.players),
            cards: [action.cards],
            leader: performedBy,
            value,
            finished: state.phase.finished,
          };
          return state;
        }
        case "Bomb":
        case "Claim":
          return null;
        default:
          assertNever(action);
      }
    }

    case "Following":
      const phase = state.phase;
      switch (action.kind) {
        case "Pass": {
          state.phase.player = getNextPlayer(
            performedBy,
            state.players,
            state.phase.leader
          );
          return state;
        }

        case "BetBig":
        case "Push":
        case "Ready":
          return null;

        case "BetSmall": {
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            user.bet = 100;
          }
          return state;
        }

        case "Play": {
          const combo = classifyCombo(action.cards);
          const value =
            combo && comboValue(combo, action.cards, state.phase.value);

          if (value === null) return null;

          const user = state.players.find((p) => p.id === performedBy);

          if (!user) return null;

          if (
            state.wish !== undefined &&
            fulfillsWish(state.wish, action.cards)
          ) {
            delete state.wish;
          }

          removeCardsFromHand(user, action.cards);
          if (handEmpty(user)) {
            state.phase.finished.push(user.id);

            const doubleVictory =
              phase.finished.length === 2 &&
              findTeam(user.id, state)?.members.every((member) =>
                phase.finished.includes(member.id)
              );

            if (doubleVictory) {
              state.phase = {
                kind: "RoundComplete",
                finished: phase.finished,
                ready: [],
              };
              return state;
            }
          }

          state.phase.value = value;
          state.phase.cards.push(action.cards);
          state.phase.leader = performedBy;
          state.phase.player = getNextPlayer(performedBy, state.players);

          return state;
        }
        case "Bomb": {
          const combo = classifyCombo(action.cards) as Bomb;
          const value = comboValue(combo, action.cards, state.phase.value);
          if (value === null) {
            return null;
          }

          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            removeCardsFromHand(user, action.cards);
          }

          state.phase.value = value;
          state.phase.cards.push(action.cards);
          state.phase.leader = performedBy;
          state.phase.player = getNextPlayer(performedBy, state.players);

          return state;
        }
        case "Claim": {
          const user = state.players.find((p) => p.id === performedBy);
          if (user) {
            const winningCombo =
              state.phase.cards[state.phase.cards.length - 1];

            if (
              classifyCombo(winningCombo) === "single" &&
              winningCombo[0] === dragon
            ) {
              const giveToUser = state.players.find(
                (p) => p.id === action.giveTo
              ) as Player;

              giveToUser.claimed.push(...state.phase.cards.flat());
            } else {
              user.claimed.push(...state.phase.cards.flat());
            }

            if (state.phase.finished.length === 3 /* round is over */) {
              state.phase = {
                kind: "RoundComplete",
                finished: state.phase.finished,
                ready: [],
              };
              return state;
            } else {
              state.phase = {
                kind: "Leading",
                player: handNotEmpty(user)
                  ? user.id
                  : getNextPlayer(user.id, state.players),
                dog: false,
                finished: state.phase.finished,
              };
              return state;
            }
          } else {
            return null;
          }
        }
        default:
          assertNever(action);
      }
    case "RoundComplete": {
      switch (action.kind) {
        case "Pass":
        case "BetBig":
        case "BetSmall":
        case "Push":
        case "Play":
        case "Bomb":
        case "Claim":
          return null;

        case "Ready": {
          const phase = state.phase;
          phase.ready.push(performedBy);

          if (state.players.every((p) => phase.ready.includes(p.id))) {
            const doubleVictory = phase.finished.length === 2;
            const teams = getTeams(state);
            if (doubleVictory) {
              const winningTeamIndex =
                teams.find((team) =>
                  phase.finished.includes(team.members[0].id)
                )?.index ?? 0;
              state.score[winningTeamIndex] += 200;
            } else {
              const loserIndex = state.players.findIndex(handNotEmpty);

              const loser = state.players[loserIndex];
              const nextToLoser =
                state.players[(loserIndex + 1) % state.players.length];

              const winner = state.players.find(
                (p) => p.id === phase.finished[0]
              ) as Player;

              winner.claimed.push(...loser.claimed);
              nextToLoser.claimed.push(...loser.hand);

              loser.hand = [];
              loser.claimed = [];

              teams.forEach((team) => {
                team.members.forEach((member) => {
                  const score = sum(member.claimed.map(getScoreValue));
                  state.score[team.index] += score;
                });
              });
            }

            teams.forEach((team) => {
              team.members.forEach((member) => {
                let betValue = member.bet ?? 0;

                if (phase.finished[0] !== member.id) {
                  betValue = -betValue;
                }

                state.score[team.index] += betValue;
              });
            });

            if (shouldGameEnd(state, settings)) {
              state.phase = {
                kind: "GameComplete",
              };
              return state;
            } else {
              /*
               * Start a new round!
               */
              const rng = seedrandom.alea(JSON.stringify(seed));

              const deck = getSortedDeck();
              shuffle(rng, deck);

              state.players.forEach((player) => {
                player.claimed = [];
                player.hand = deck.splice(0, 8);
                delete player.bet;
              });

              state.phase = {
                kind: "BigBetting",
                deck,
                ready: [],
              };

              delete state.wish;

              return state;
            }
          } else {
            return state;
          }
        }
      }
    }
    case "GameComplete": {
      return null;
    }
    default:
      return assertNever(state.phase);
  }
}

function shouldGameEnd(state: GameState, settings: GameSettings): boolean {
  const hasReachedScore = (() => {
    switch (settings.gameLength) {
      case "standard-game":
        return state.score.some((s) => s >= 1000);
      case "shorter-game":
        return state.score.some((s) => s >= 500);
      case "single-round":
        return true;
    }
  })();

  const mustPlayAnother = (() => {
    switch (settings.tiebreaker) {
      case "play-another-round":
        return state.score[0] === state.score[1];
      case "no-tiebreaker":
        return false;
    }
  })();

  return hasReachedScore && !mustPlayAnother;
}

const hasPlayerId =
  (playerId: PlayerId) =>
  (player: Player): boolean =>
    player.id === playerId;

function handEmpty(player: Player): boolean {
  return player.hand.length === 0;
}
function handNotEmpty(player: Player): boolean {
  return !handEmpty(player);
}

function getDogTarget(userId: PlayerId, players: Player[]): PlayerId {
  const userIdx = players.findIndex(hasPlayerId(userId));
  const targets = [
    players[(userIdx + 2) % players.length],
    players[(userIdx + 3) % players.length],
    players[(userIdx + 1) % players.length],
  ];
  const target = targets.find(handNotEmpty);
  if (target !== undefined) {
    return target.id;
  } else {
    /* this should never happen */
    throw new Error("no valid dog target found");
  }
}

function getNextPlayer(
  userId: PlayerId,
  players: Player[],
  leader?: PlayerId
): PlayerId {
  const userIdx = players.findIndex((p) => p.id === userId);
  const reorderedOtherPlayers = [
    ...players.slice(userIdx + 1),
    ...players.slice(0, userIdx),
  ];
  const nextPlayer = reorderedOtherPlayers.find(
    (p) => p.id === leader || handNotEmpty(p)
  );

  return nextPlayer?.id ?? userId;
}

function removeCardsFromHand(player: Player, cards: Card[]): void {
  for (const cardToRemove of cards) {
    const idx = player.hand.indexOf(cardToRemove);
    if (idx !== undefined) {
      player.hand.splice(idx, 1);
    }
  }
}

function completeBigBettingPhase(state: GameState, phase: BigBetting): void {
  for (const player of state.players) {
    const cards = phase.deck.splice(0, 6);
    cards.forEach((card) => player.hand.push(card));
  }

  state.phase = {
    kind: "Pushing",
    pushed: {},
  };
}
