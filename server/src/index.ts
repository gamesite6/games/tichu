import type { Action, PlayerId, GameSettings, GameState } from "./shared";
import fastify, * as f from "fastify";
import initialState from "./initialState";
import performAction, { isCompleted } from "./performAction";

type InfoReq = {
  settings: GameSettings;
};
type InfoRes = {
  playerCounts: number[];
};

type InitialStateReq = {
  players: PlayerId[];
  settings: GameSettings;
  seed: number;
};
type InitialStateRes = {
  state: GameState;
};
type PerformActionReq = {
  settings: GameSettings;
  state: GameState;
  action: Action;
  seed: number;
  performedBy: PlayerId;
};
type PerformActionRes = {
  completed: boolean;
  nextState: GameState;
};

const server = fastify({
  logger: {
    level: "warn",
  },
});

const port = parseInt(process.env.PORT || "");

if (!port) {
  console.error("PORT not specified");
  process.exit(1);
}

const infoOpts: f.RouteShorthandOptions = {
  schema: {
    body: {
      type: "object",
      properties: {
        settings: {},
      },
    },
    response: {
      200: {
        type: "object",
        properties: {
          playerCounts: {
            type: "array",
            items: { type: "number" },
          },
        },
      },
    },
  },
};

server.post("/info", infoOpts, async (request, reply) => {
  const res: InfoRes = { playerCounts: [4] };
  return res;
});

const initialStateOpts: f.RouteShorthandOptions = {
  schema: {
    body: {
      type: "object",
      properties: {
        players: {
          type: "array",
          items: { type: "number" },
        },
        settings: {},
        seed: { type: "number" },
      },
    },
    response: {
      200: {
        type: "object",
        properties: {
          state: {},
        },
      },
    },
  },
};

server.post("/initial-state", initialStateOpts, async (request, reply) => {
  const req = request.body as InitialStateReq;

  const state = initialState(req.players, req.seed);

  if (!state) {
    throw { statusCode: 422 };
  }

  const res: InitialStateRes = { state };
  return res;
});

const performActionOpts: f.RouteShorthandOptions = {
  schema: {
    body: {
      type: "object",
      properties: {
        performedBy: { type: "number" },
        action: {},
        state: {},
        settings: {},
        seed: { type: "number" },
      },
    },
    response: {
      200: {
        type: "object",
        properties: {
          completed: { type: "boolean" },
          nextState: {},
        },
      },
    },
  },
};

server.post("/perform-action", performActionOpts, async (request, reply) => {
  const req = request.body as PerformActionReq;

  const nextState = performAction(req);
  if (!nextState) {
    throw { statusCode: 422 };
  }

  const res: PerformActionRes = {
    completed: isCompleted(nextState),
    nextState,
  };
  return res;
});

server.listen(port, "0.0.0.0", (err) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }

  console.info(`server listening on port ${port}`);
});
