import {
  Card,
  cards as sortedDeck,
  Player,
  PlayerId,
  GameState,
} from "./shared";
import * as seedrandom from "seedrandom";
import { shuffle } from "./util";

export function getSortedDeck(): Card[] {
  return [...sortedDeck];
}

export default function initialState(
  playerIds: PlayerId[],
  seed: number
): GameState | null {
  const playerIdSet = new Set(playerIds);
  if (playerIdSet.size != 4) {
    return null;
  }

  const rng = seedrandom.alea(JSON.stringify(seed));

  const deck = getSortedDeck();
  shuffle(rng, deck);

  const players: Player[] = playerIds.map((playerId) => ({
    id: playerId,
    hand: deck.splice(0, 8),
    claimed: [],
  }));

  const state: GameState = {
    players,
    score: [0, 0],
    phase: { kind: "BigBetting", ready: [], deck },
  };

  return state;
}
